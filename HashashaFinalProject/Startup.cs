﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(HashashaSPA.Startup))]

namespace HashashaSPA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
