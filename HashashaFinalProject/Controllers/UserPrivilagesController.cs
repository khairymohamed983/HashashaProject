﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HashashaSPA.Models;

namespace HashashaSPA.Controllers
{
    //[HashAuthorize(PrivilageId = -1)]
    public class UserPrivilagesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/UserPrivilages
        public IQueryable<UserPrivilage> GetUserPrivilages()
        {
            return db.UserPrivilages;
        }

        // GET: api/UserPrivilages/5
        [ResponseType(typeof(UserPrivilage))]
        public async Task<IHttpActionResult> GetUserPrivilage(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<UserPrivilage> userPrivilage = db.UserPrivilages.Where(a => a.Id == id).ToList();
            if (userPrivilage == null)
            {
                return NotFound();
            }

            return Ok(userPrivilage);
        }

        // PUT: api/UserPrivilages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserPrivilage(string id, UserPrivilage userPrivilage)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userPrivilage.Id)
            {
                return BadRequest();
            }

            db.Entry(userPrivilage).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserPrivilageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserPrivilages
        [ResponseType(typeof(UserPrivilage))]
        public async Task<IHttpActionResult> PostUserPrivilage(UserPrivilage userPrivilage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserPrivilages.Add(userPrivilage);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserPrivilageExists(userPrivilage.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = userPrivilage.Id }, userPrivilage);
        }

        // DELETE: api/UserPrivilages/5
        [ResponseType(typeof(UserPrivilage))]
        public async Task<IHttpActionResult> DeleteUserPrivilage(string id)
        {
            UserPrivilage userPrivilage = await db.UserPrivilages.FindAsync(id);
            if (userPrivilage == null)
            {
                return NotFound();
            }

            db.UserPrivilages.Remove(userPrivilage);
            await db.SaveChangesAsync();

            return Ok(userPrivilage);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserPrivilageExists(string id)
        {
            return db.UserPrivilages.Count(e => e.Id == id) > 0;
        }
    }
}