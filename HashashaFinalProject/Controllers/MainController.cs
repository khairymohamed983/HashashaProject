﻿using CrystalDecisions.CrystalReports.Engine;
using HashashaSPA.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CrystalDecisions.Shared.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO.Compression;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace HashashaSPA.Controllers
{
    [Authorize]
    public class MainController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        string[] education = { "Un_educated", "Read_Write", "Primary", "Intermediate", "Secondary", "University" };
        string[] CaseStatus = { "OsteoporosisWithFracture", "Osteoporosis", "Osteopenia", "Normal" };
        string[] occupationArr = { "Unemployed", "Student", "Professional", "Manual_Labor", "Unknown", "Other_Specify" };

        // GET: Main
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName("GetStastics")]
        public ActionResult GetStastics()
        {

            var patientsCount = ListOfPatients().Count(x => x.isDeleted == false);
            var usersCount = db.Users.Count(x => x.isDeleted == false);
            var rolesCount = db.Roles.Count(x => x.isDeleted == false);
            var citiesCount = db.Cities.Count(x => x.isDeleted == false);
            // --------------------------number patient that have Osteoporosis---------------------------------
            var case0Count = db.Patients.Count(a => a.OsteoporosisCase == 0);
            var case1Count = db.Patients.Count(a => a.OsteoporosisCase == 1);
            var case2Count = db.Patients.Count(a => a.OsteoporosisCase == 2);
            var case3Count = db.Patients.Count(a => a.OsteoporosisCase == 3);

            var retData = new
            {
                items = new[]
            {
                new { label= "OsteoporosisWithFracture", value= case0Count},
                new { label= "Osteoporosis", value= case1Count},
                new { label= "Osteopenia", value= case2Count},
                new { label= "Normal", value= case3Count},
                }
            };

            // -----------------------------Diagnosed cases of osteoporosis for Gender------------------------------
            var osteoWithFractureMale = db.Patients.Count(a => a.OsteoporosisCase == 0 && a.PatientGender);
            var osteoWithFractureFemale = db.Patients.Count(a => a.OsteoporosisCase == 0 && a.PatientGender == false);
            var OsteoporosisMale = db.Patients.Count(a => a.OsteoporosisCase == 1 && a.PatientGender);
            var OsteoporosisFemale = db.Patients.Count(a => a.OsteoporosisCase == 1 && a.PatientGender == false);
            var OsteopeniaMale = db.Patients.Count(a => a.OsteoporosisCase == 2 && a.PatientGender);
            var OsteopeniaFemale = db.Patients.Count(a => a.OsteoporosisCase == 2 && a.PatientGender == false);
            var NormalMale = db.Patients.Count(a => a.OsteoporosisCase == 2 && a.PatientGender);
            var NormalFemale = db.Patients.Count(a => a.OsteoporosisCase == 2 && a.PatientGender == false);

            var caseWithGender = new
            {
                items = new[]
               {
                new { label= "OsteoporosisWithFracture", valueMale= osteoWithFractureMale, valueFemale=osteoWithFractureFemale},
                new { label= "Osteoporosis", valueMale= OsteoporosisMale , valueFemale = OsteoporosisFemale},
                new { label= "Osteopenia", valueMale= OsteopeniaMale, valueFemale= OsteopeniaFemale},
                new { label= "Normal", valueMale= NormalMale, valueFemale= NormalFemale},
                }
            };

            // -----------------------------number patient in each city------------------------------
            var dictionary = new Dictionary<string, int>();
            var allCities = db.Cities.Where(c => c.isDeleted == false);
            foreach (var city in allCities)
            {
                var patientCount = db.Patients.Count(p => p.ServiceCityId == city.CityId);
                var cityName = city.CityName;
                dictionary.Add(cityName, patientCount);
            }
            var patientInCity = dictionary.FromDictionaryToJson();

            // -----------------------------The risk factor for cases of diagnosis------------------------------
            // LowVitamin
            var lowVitaminosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("LowVitamin") && p.OsteoporosisCase == 0);
            var lowVitaminOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("LowVitamin") && p.OsteoporosisCase == 1);
            var LowVitaminOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("LowVitamin") && p.OsteoporosisCase == 2);
            var lowVitaminNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("LowVitamin") && p.OsteoporosisCase == 3);
            //HormoneTherapy 
            var hormoneTherapyosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("HormoneTherapy") && p.OsteoporosisCase == 0);
            var hormoneTherapyOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("HormoneTherapy") && p.OsteoporosisCase == 1);
            var hormoneTherapyOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("HormoneTherapy") && p.OsteoporosisCase == 2);
            var hormoneTherapyNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("HormoneTherapy") && p.OsteoporosisCase == 3);
            //Smoking 
            var smokingosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("Smoking") && p.OsteoporosisCase == 0);
            var smokingOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("Smoking") && p.OsteoporosisCase == 1);
            var smokingOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("Smoking") && p.OsteoporosisCase == 2);
            var smokingNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("Smoking") && p.OsteoporosisCase == 3);
            //RheumatoidArthritis 
            var rheumatoidArthritisosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("RheumatoidArthritis") && p.OsteoporosisCase == 0);
            var rheumatoidArthritisOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("RheumatoidArthritis") && p.OsteoporosisCase == 1);
            var rheumatoidArthritisOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("RheumatoidArthritis") && p.OsteoporosisCase == 2);
            var rheumatoidArthritisNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("RheumatoidArthritis") && p.OsteoporosisCase == 3);
            //Glucocorticoid 
            var glucocorticoidosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("Glucocorticoid") && p.OsteoporosisCase == 0);
            var glucocorticoidOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("Glucocorticoid") && p.OsteoporosisCase == 1);
            var glucocorticoidOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("Glucocorticoid") && p.OsteoporosisCase == 2);
            var glucocorticoidNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("Glucocorticoid") && p.OsteoporosisCase == 3);
            //SecondryCauses 
            var secondryCausesosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("SecondryCauses") && p.OsteoporosisCase == 0);
            var secondryCausesOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("SecondryCauses") && p.OsteoporosisCase == 1);
            var secondryCausesOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("SecondryCauses") && p.OsteoporosisCase == 2);
            var secondryCausesNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("SecondryCauses") && p.OsteoporosisCase == 3);
            //PersonalHistoryOfFragilityFracture
            var personalHistoryosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("PersonalHistoryOfFragilityFracture") && p.OsteoporosisCase == 0);
            var personalHistoryOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("PersonalHistoryOfFragilityFracture") && p.OsteoporosisCase == 1);
            var personalHistoryOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("PersonalHistoryOfFragilityFracture") && p.OsteoporosisCase == 2);
            var personalHistoryNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("PersonalHistoryOfFragilityFracture") && p.OsteoporosisCase == 3);
            //FamilyHistoryOfOsteoporoticFracture
            var familyHistoryosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("FamilyHistoryOfOsteoporoticFracture") && p.OsteoporosisCase == 0);
            var familyHistoryOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("FamilyHistoryOfOsteoporoticFracture") && p.OsteoporosisCase == 1);
            var familyHistoryOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("FamilyHistoryOfOsteoporoticFracture") && p.OsteoporosisCase == 2);
            var familyHistoryNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("FamilyHistoryOfOsteoporoticFracture") && p.OsteoporosisCase == 3);
            //lowCalciumIntakelesThanTwoServings
            var lowCalciumosteoWithFracture = db.Patients.Count(p => p.BodyMassIndex.Contains("LowCalciumIntakelesThanTwoServings") && p.OsteoporosisCase == 0);
            var lowCalciumOsteoporosis = db.Patients.Count(p => p.BodyMassIndex.Contains("LowCalciumIntakelesThanTwoServings") && p.OsteoporosisCase == 1);
            var lowCalciumOsteopenia = db.Patients.Count(p => p.BodyMassIndex.Contains("LowCalciumIntakelesThanTwoServings") && p.OsteoporosisCase == 2);
            var lowCalciumNormal = db.Patients.Count(p => p.BodyMassIndex.Contains("LowCalciumIntakelesThanTwoServings") && p.OsteoporosisCase == 3);

            var riskFactorCases = new
            {
                items = new[]
              {
                new { label= "LowVitamin", value1= lowVitaminosteoWithFracture, value2=lowVitaminOsteoporosis, value3=LowVitaminOsteopenia, value4=lowVitaminNormal},
                new { label= "HormoneTherapy", value1= hormoneTherapyosteoWithFracture, value2=hormoneTherapyOsteoporosis, value3=hormoneTherapyOsteopenia, value4=hormoneTherapyNormal},
                new { label= "Smoking", value1= smokingosteoWithFracture, value2=smokingOsteoporosis, value3=smokingOsteopenia, value4=smokingNormal},
                new { label= "RheumatoidArthritis", value1= rheumatoidArthritisosteoWithFracture, value2=rheumatoidArthritisOsteoporosis, value3=rheumatoidArthritisOsteopenia, value4=rheumatoidArthritisNormal},
                new { label= "Glucocorticoid", value1= glucocorticoidosteoWithFracture, value2=glucocorticoidOsteoporosis, value3=glucocorticoidOsteopenia, value4=glucocorticoidNormal},
                new { label= "SecondryCauses", value1= secondryCausesosteoWithFracture, value2=secondryCausesOsteoporosis, value3=secondryCausesOsteopenia, value4=secondryCausesNormal},
                new { label= "PersonalHistoryOfFragilityFracture", value1= personalHistoryosteoWithFracture, value2=personalHistoryOsteoporosis, value3=personalHistoryOsteopenia, value4=personalHistoryNormal},
                new { label= "FamilyHistoryOfOsteoporoticFracture", value1= familyHistoryosteoWithFracture, value2=familyHistoryOsteoporosis, value3=familyHistoryOsteopenia, value4=familyHistoryNormal},
                new { label= "lowCalciumIntakelesThanTwoServings", value1= lowCalciumosteoWithFracture, value2=lowCalciumOsteoporosis, value3=lowCalciumOsteopenia, value4=lowCalciumNormal}
                }
            };


            return Json(new
            {
                DonutchartsData = retData,
                BarGenderData = caseWithGender,
                PatientInCity = patientInCity,
                PatientsNum = patientsCount,
                RiskFactorCases = riskFactorCases,
                UsersNum = usersCount,
                RolesNum = rolesCount,
                CitiesNum = citiesCount
            }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName("GetCharts")]
        public ActionResult GetCharts()
        {
            // -----------------------------Gender and education EPG ------------------------------ 
            var boolVal = new[] { true, false };
            int EPG;
            var dict = new Dictionary<string, List<int>>();
            List<int> list = new List<int>();

            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 6; j++)
                {
                    foreach (var t in boolVal)
                    {
                        EPG = db.Patients.AsEnumerable().Count(p => p.OsteoporosisCase == i && p.Education == j && p.PatientGender == t);

                        switch (i)
                        {
                            case 0:
                                if (!dict.TryGetValue("OsteoporosisWithFracture", out list))
                                {
                                    list = new List<int>();
                                    dict.Add("OsteoporosisWithFracture", list);
                                }
                                list.Add(EPG);
                                break;
                            case 1:
                                if (!dict.TryGetValue("Osteoporosis", out list))
                                {
                                    list = new List<int>();
                                    dict.Add("Osteoporosis", list);
                                }
                                list.Add(EPG);
                                break;
                            case 2:
                                if (!dict.TryGetValue("Osteopenia", out list))
                                {
                                    list = new List<int>();
                                    dict.Add("Osteopenia", list);
                                }
                                list.Add(EPG);
                                break;
                            case 3:
                                if (!dict.TryGetValue("Normal", out list))
                                {
                                    list = new List<int>();
                                    dict.Add("Normal", list);
                                }
                                list.Add(EPG);
                                break;
                        }
                    }
                }
            }
            return Json(new { EPG = dict }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("GetMapData")]
        public ActionResult GetMapData()
        {
            var dictionary = new Dictionary<string, int>();
            var dictionary1 = new Dictionary<string, int>();
            var allCities = db.Cities.Where(c => c.isDeleted == false);
            foreach (var city in allCities)
            {
                var patientMaleCount = db.Patients.Count(p => p.ServiceCityId == city.CityId && p.PatientGender);
                var patientFemaleCount = db.Patients.Count(p => p.ServiceCityId == city.CityId && p.PatientGender == false);
                var cityName = city.CityName;
                dictionary.Add(cityName, patientMaleCount);
                dictionary1.Add(cityName, patientFemaleCount);
            }
            var patientMaleInCity = dictionary.FromDictionaryToJson();
            var patientFemaleInCity = dictionary1.FromDictionaryToJson();

            return Json(new { PatientMaleInCity = patientMaleInCity, PatientFemaleInCity = patientFemaleInCity }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("UploadFile")]
        public ActionResult UploadFile(HttpPostedFileWrapper fileUploader)
        {
            string virtualPath = "";
            if (fileUploader != null)
            {
                string FullPath = Path.Combine(Server.MapPath("~/Files"), fileUploader.FileName);
                fileUploader.SaveAs(FullPath);
                virtualPath = "~/Files/" + fileUploader.FileName;
            }
            return Json(new { fileUrl = virtualPath }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("Download")]
        public ActionResult Download(int? id)
        {
            try
            {
                var patient = db.Patients.FirstOrDefault(f => f.PatientId == id);
                if (!string.IsNullOrEmpty(patient.FileUrl))
                {
                    var pid = patient.PatientId;
                    var dir = Server.MapPath("~/SysFiles/PatientFiles/" + pid);
                    var zipDir = Server.MapPath("~/SysFiles/PatientFiles/" + pid + ".zip");
                    CreateSample(zipDir, "", dir);
                    return File(zipDir, "application/zip");
                }
                return Json(new { message = "No Files Available for the Patient" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
                // throw;
            }

        }

        public static void CreateSample(string outPathname, string password, string folderName)
        {
            FileStream fsOut = System.IO.File.Create(outPathname);
            ZipOutputStream zipStream = new ZipOutputStream(fsOut);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

            //zipStream.Password = password;  // optional. Null is the same as not setting. Required if using AES.

            // This setting will strip the leading part of the folder path in the entries, to
            // make the entries relative to the starting folder.
            // To include the full path for each entry up to the drive root, assign folderOffset = 0.
            int folderOffset = folderName.Length + (folderName.EndsWith("\\") ? 0 : 1);

            CompressFolder(folderName, zipStream, folderOffset);

            zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
            zipStream.Close();
        }

        // Recurses down the folder structure
        //
        private static void CompressFolder(string path, ZipOutputStream zipStream, int folderOffset)
        {
            string[] files = Directory.GetFiles(path);
            foreach (string filename in files)
            {
                FileInfo fi = new FileInfo(filename);
                string entryName = filename.Substring(folderOffset); // Makes the name in zip based on the folder
                entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
                ZipEntry newEntry = new ZipEntry(entryName);
                newEntry.DateTime = fi.LastWriteTime; // Note the zip format stores 2 second granularity

                // Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
                // A password on the ZipOutputStream is required if using AES.
                //   newEntry.AESKeySize = 256;
                // To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                // you need to do one of the following: Specify UseZip64.Off, or set the Size.
                // If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                // but the zip will be in Zip64 format which not all utilities can understand.
                //   zipStream.UseZip64 = UseZip64.Off;
                newEntry.Size = fi.Length;

                zipStream.PutNextEntry(newEntry);

                // Zip the file in buffered chunks
                // the "using" will close the stream even if an exception occurs
                byte[] buffer = new byte[4096];
                using (FileStream streamReader = System.IO.File.OpenRead(filename))
                {
                    StreamUtils.Copy(streamReader, zipStream, buffer);
                }
                zipStream.CloseEntry();
            }

            string[] folders = Directory.GetDirectories(path);
            foreach (string folder in folders)
            {
                CompressFolder(folder, zipStream, folderOffset);
            }
        }


        [HttpGet]
        [ActionName("PatientReport")]
        public ActionResult PatientReport(int? id)
        {
            if (id == null)
            {
                return View("Index");
            }

            ReportDocument doc = new ReportDocument();
            doc.Load(Server.MapPath("~/Reports/PatientReport.rpt"));

            var patient = db.Patients.FirstOrDefault(a => a.PatientId == id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            var BMI = patient.BodyMassIndex.Replace(',', '-');
            var liveCity = db.Cities.FirstOrDefault(l => l.CityId == patient.LiveCityId)?.CityName;
            var serviceCity = db.Cities.FirstOrDefault(l => l.CityId == patient.ServiceCityId)?.CityName;

            doc.Database.Tables["PatientData"].SetDataSource(new[]
            {
                new {
                    PatientName = $"{patient.PatientFirstName} {patient.PatientFatherName} {patient.PatientLastName}",
                    Gender = patient.PatientGender ? "Male" : "Female",
                    NationalId = patient.NationalId,
                    TestDate =patient.TestDate.Day + "/" +patient.TestDate.Month + "/"+patient.TestDate.Year ,
                    BirthDate =patient.BirthDate.Day + "/" +patient.BirthDate.Month + "/"+patient.BirthDate.Year,
                    CreationDate = patient.CreationDate,
                    ContactNumber = patient.ContactNumber,
                    MobileNumber = patient.MobileNumber,
                    Occupation = occupationArr[patient.Occupation],
                    Education = education[patient.Education],
                    RiskFactor = patient.RiskFactor,
                    Height = patient.Height,
                    Weight = patient.Weight,
                    BMI = BMI,
                    HasFracture = patient.HasFracture ? "Yes" : "No",
                    DXALumber = patient.DXALumber,
                    DXAFemoral = patient.DXAFemoral,
                    DXATotalHip = patient.DXATotalHip,
                    BMDLumber = patient.BMDLumber,
                    BMDFemoral = patient.BMDFemoral,
                    BMDTotalHip = patient.BMDTotalHip,
                    OsteoporosisCase = CaseStatus[patient.OsteoporosisCase],
                    ServiceCity = serviceCity,
                    LiveCity = liveCity
              } });

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream strm = doc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            strm.Seek(0, SeekOrigin.Begin);
            Response.AppendHeader("content-disposition", "inline;filename=Patient" + patient.PatientFirstName + " " + patient.PatientFatherName + " " + patient.PatientLastName + ".pdf");

            return File(strm, "application/pdf");
        }

        public List<Patient> Filter(FilterPatientViewModel vm)
        {
            if (vm == null)
            {
                return db.Patients.ToList();
            }

            db.Configuration.ProxyCreationEnabled = false; List<Patient> data = new List<Patient>();
            //var data = db.Patients.Where(a => (
            //                              !string.IsNullOrEmpty(vm.PatientFirstName) ? a.PatientFirstName.Contains(vm.PatientFirstName) : a.PatientFirstName != vm.PatientFirstName)
            //                              && (!string.IsNullOrEmpty(vm.PatientFatherName) ? a.PatientFatherName.Contains(vm.PatientFatherName) : a.PatientFatherName != vm.PatientFatherName)
            //                              && (!string.IsNullOrEmpty(vm.PatientLastName) ? a.PatientLastName.Contains(vm.PatientLastName) : a.PatientLastName != vm.PatientLastName)
            //                              && (vm.PatientGender != null ? a.PatientGender == vm.PatientGender : a.PatientGender != vm.PatientGender)
            //                              && (!string.IsNullOrEmpty(vm.NationalId) ? a.NationalId.Contains(vm.NationalId) : a.NationalId != vm.NationalId)
            //                              && (!string.IsNullOrEmpty(vm.ContactNumber) ? a.ContactNumber.Contains(vm.ContactNumber) : a.ContactNumber != vm.ContactNumber)
            //                              && (!string.IsNullOrEmpty(vm.MobileNumber) ? a.MobileNumber.Contains(vm.MobileNumber) : a.MobileNumber != vm.MobileNumber)
            //                              && (!string.IsNullOrEmpty(vm.BodyMassIndex) ? a.BodyMassIndex.Contains(vm.BodyMassIndex) : a.BodyMassIndex != vm.BodyMassIndex)
            //                              && (vm.Height != null ? a.Height == vm.Height : a.Height != vm.Height)
            //                              && (vm.Weight != null ? a.Weight == vm.Weight : a.Weight != vm.Weight)
            //                              && (vm.BMDFemoral != null ? a.BMDFemoral == vm.BMDFemoral : a.Height != vm.BMDFemoral)
            //                              && (vm.BMDLumber != null ? a.BMDLumber == vm.BMDLumber : a.BMDLumber != vm.BMDLumber)
            //                              && (vm.BMDTotalHip != null ? a.BMDTotalHip == vm.BMDTotalHip : a.BMDTotalHip != vm.BMDTotalHip)
            //                              && (vm.DXAFemoral != null ? a.DXAFemoral == vm.DXAFemoral : a.DXAFemoral != vm.DXAFemoral)
            //                              && (vm.DXALumber != null ? a.DXALumber == vm.DXALumber : a.DXALumber != vm.DXALumber)
            //                              && (vm.DXATotalHip != null ? a.DXATotalHip == vm.DXATotalHip : a.DXATotalHip != vm.DXATotalHip)
            //                              && (vm.BirthDateFrom != null && vm.BirthDateTo != null ? a.BirthDate >= vm.BirthDateFrom && a.BirthDate <= vm.BirthDateTo : a.BirthDate != vm.BirthDateFrom)
            //                              && (vm.TestDateFrom != null && vm.TestDateTo != null ? a.TestDate >= vm.TestDateFrom && a.TestDate <= vm.TestDateTo : a.TestDate != vm.TestDateFrom)
            //                              && (vm.Occupation > 0 ? a.Occupation == vm.Occupation : a.Occupation != vm.Occupation)
            //                              && (vm.Education > 0 ? a.Education == vm.Education : a.Education != vm.Education)
            //                              && (vm.OsteoporosisCase > 0 ? a.OsteoporosisCase == vm.OsteoporosisCase : a.OsteoporosisCase != vm.OsteoporosisCase)
            //                              && (vm.HasFracture != null ? a.HasFracture == vm.HasFracture : a.HasFracture != vm.HasFracture)
            //                              && (vm.LiveCityId != null ? a.LiveCityId == vm.LiveCityId : a.LiveCityId != vm.LiveCityId)
            //                              && (vm.ServiceCityId != null ? a.ServiceCityId == vm.ServiceCityId : a.ServiceCityId != vm.ServiceCityId)
            //                              && (vm.FromAge > 0 && vm.ToAge > 0 ? (DateTime.Now.Year - a.BirthDate.Year) >= vm.FromAge && (DateTime.Now.Year - a.BirthDate.Year) <= vm.ToAge : a.BirthDate != vm.TestDateFrom)

            //                              );
            foreach (Patient patient in db.Patients)
            {

                bool x = (!string.IsNullOrEmpty(vm.PatientFirstName) ? patient.PatientFirstName.Contains(vm.PatientFirstName) : patient.PatientFirstName != vm.PatientFirstName);
                bool x1 = (!string.IsNullOrEmpty(vm.PatientFatherName) ? patient.PatientFatherName.Contains(vm.PatientFatherName) : patient.PatientFatherName != vm.PatientFatherName);
                bool x2 = (!string.IsNullOrEmpty(vm.PatientLastName) ? patient.PatientLastName.Contains(vm.PatientLastName) : patient.PatientLastName != vm.PatientLastName);
                bool x3 = (vm.PatientGender != null ? patient.PatientGender == vm.PatientGender : patient.PatientGender != vm.PatientGender);

                bool x4 = (!string.IsNullOrEmpty(vm.NationalId) ? patient.NationalId.Contains(vm.NationalId) : patient.NationalId != vm.NationalId);
                bool x5 = (!string.IsNullOrEmpty(vm.ContactNumber) ? patient.ContactNumber.Contains(vm.ContactNumber) : patient.ContactNumber != vm.ContactNumber);
                bool x6 = (!string.IsNullOrEmpty(vm.MobileNumber) ? patient.MobileNumber.Contains(vm.MobileNumber) : patient.MobileNumber != vm.MobileNumber);
                bool x7 = (!string.IsNullOrEmpty(vm.BodyMassIndex) ? patient.BodyMassIndex.Contains(vm.BodyMassIndex) : patient.BodyMassIndex != vm.BodyMassIndex);
                bool x8 = (vm.Height != null ? patient.Height == vm.Height : patient.Height != vm.Height);
                bool x9 = (vm.Weight != null ? patient.Weight == vm.Weight : patient.Weight != vm.Weight);
                bool y0 = (vm.BMDFemoral != null ? patient.BMDFemoral == vm.BMDFemoral : patient.Height != vm.BMDFemoral);
                bool y1 = (vm.BMDLumber != null ? patient.BMDLumber == vm.BMDLumber : patient.BMDLumber != vm.BMDLumber);
                bool y2 = (vm.BMDTotalHip != null ? patient.BMDTotalHip == vm.BMDTotalHip : patient.BMDTotalHip != vm.BMDTotalHip);
                bool y3 = (vm.DXAFemoral != null ? patient.DXAFemoral == vm.DXAFemoral : patient.DXAFemoral != vm.DXAFemoral);
                bool y4 = (vm.DXALumber != null ? patient.DXALumber == vm.DXALumber : patient.DXALumber != vm.DXALumber);
                bool y5 = (vm.DXATotalHip != null ? patient.DXATotalHip == vm.DXATotalHip : patient.DXATotalHip != vm.DXATotalHip);
                bool y6 = (vm.BirthDateFrom != null && vm.BirthDateTo != null ? patient.BirthDate >= vm.BirthDateFrom && patient.BirthDate <= vm.BirthDateTo : patient.BirthDate != vm.BirthDateFrom);
                bool y7 = (vm.TestDateFrom != null && vm.TestDateTo != null ? patient.TestDate >= vm.TestDateFrom && patient.TestDate <= vm.TestDateTo : patient.TestDate != vm.TestDateFrom);
                bool y8 = (vm.Occupation > 0 ? patient.Occupation == vm.Occupation : patient.Occupation != vm.Occupation);
                bool y9 = (vm.Education > 0 ? patient.Education == vm.Education : patient.Education != vm.Education);
                bool z0 = (vm.OsteoporosisCase > 0 ? patient.OsteoporosisCase == vm.OsteoporosisCase : patient.OsteoporosisCase != vm.OsteoporosisCase);
                bool z1 = (vm.HasFracture != null ? patient.HasFracture == vm.HasFracture : patient.HasFracture != vm.HasFracture);
                bool z2 = (vm.LiveCityId != null ? patient.LiveCityId == vm.LiveCityId : patient.LiveCityId != vm.LiveCityId);
                bool z3 = (vm.ServiceCityId != null ? patient.ServiceCityId == vm.ServiceCityId : patient.ServiceCityId != vm.ServiceCityId);
                bool z4 = (vm.FromAge > 0 && vm.ToAge > 0 ? (DateTime.Now.Year - patient.BirthDate.Year) >= vm.FromAge && (DateTime.Now.Year - patient.BirthDate.Year) <= vm.ToAge : patient.BirthDate != vm.TestDateFrom);



                if (
                    x &&
                    x1 &&
                    x2 &&
                    x3 &&
                    x4 &&
                    x5 &&
                    x6 &&
                    x7 &&
                    x8 &&
                    x9 &&
                    y0 &&
                    y1 &&
                    y2 &&
                    y3 &&
                    y4 &&
                    y5 &&
                    y6 &&
                    y7 &&
                    y8 &&
                    y9 &&
                    z0 &&
                    z1 &&
                    z2 &&
                    z3 &&
                    z4)
                    data.Add(patient);




            }
            return data;
        }

        [HttpGet]
        [ActionName("FilterPatientReport")]
        public ActionResult FilterPatientReport(string id)
        {
            var keys = id.Split(',');
            var FileType = int.Parse(keys[0]);
            List<Patient> lst = new List<Patient>();
            for (int i = 1; i < keys.Length; i++)
            {
                var idint = int.Parse(keys[i]);
                var patient = db.Patients.FirstOrDefault(a => a.PatientId == idint);
                lst.Add(patient);
            }

            var reportdata = (from x in lst
                              select new
                              {
                                  PatientFullName = $"{x.PatientFirstName} {x.PatientFatherName} {x.PatientLastName}",
                                  NationalId = x.NationalId,
                                  OsteoporosisCase = CaseStatus[x.OsteoporosisCase],
                                  ServiceCity = db.Cities.FirstOrDefault(l => l.CityId == x.ServiceCityId)?.CityName ?? "NotFound",
                                  LiveCity = db.Cities.FirstOrDefault(l => l.CityId == x.LiveCityId)?.CityName ?? "Notfound"
                              });
            ReportDocument doc = new ReportDocument();
            doc.Load(Server.MapPath("~/Reports/PatientFilter.rpt"));
            doc.Database.Tables["FilterData"].SetDataSource(reportdata);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream strm;
            string ext;
            switch (FileType)
            {
                case 1:
                    strm = doc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    ext = "pdf";
                    break;
                case 2:
                    strm = doc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    ext = "xls";
                    break;
                case 3:
                    strm = doc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
                    ext = "doc";
                    break;
                default:
                    strm = doc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    ext = "pdf";
                    break;
            }
            var fileName = "exportedFile_" + Guid.NewGuid() + "." + ext;
            strm.Seek(0, SeekOrigin.Begin);
            string contentType = ext;
            switch (ext)
            {
                case "xls":
                    contentType = "vnd.ms-excel";
                    break;
                case "doc":
                    contentType = "msword";
                    break;
            }
            return File(strm, "application/" + contentType, fileName);
        }

        [HttpPost]
        [ActionName("ShowReportResult")]
        public ActionResult ShowReportResult(FilterPatientViewModel vm)
        {
            if (vm == null)
            {
                return Json("NotFound");
            }
            var filterResult = Filter(vm);


            var patient = (from item in filterResult
                           let liveCity = db.Cities.FirstOrDefault(c => c.CityId == item.LiveCityId)?.CityName
                           let serviceCity = db.Cities.FirstOrDefault(c => c.CityId == item.ServiceCityId)?.CityName
                           select new PatientViewModel
                           {
                               Id = item.PatientId,
                               PatientFirstName = item.PatientFirstName,
                               PatientLastName = item.PatientLastName,
                               PatientFatherName = item.PatientFatherName,
                               LiveCity = liveCity,
                               ServiceCity = serviceCity,
                               OsteoporosisCase = CaseStatus[item.OsteoporosisCase],
                               NationalId = item.NationalId
                           }).ToList();

            return Json(patient);
        }

        [HttpGet]
        [ActionName("GetCurrentUser")]
        public ActionResult GetCurrentUser()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userId = db.Users.FirstOrDefault(a => a.UserName == User.Identity.Name);
            return Json(userId, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("GetAdminCity")]
        public ActionResult GetAdminCity()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userCityId = db.Users.FirstOrDefault(a => a.UserName == User.Identity.Name).ServiceCityId;
            var adminCity = db.Cities.FirstOrDefault(c => c.CityId == userCityId);
            return Json(adminCity, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName("GetPatientInCity")]
        public ActionResult GetPatientInCity()
        {
            try
            {
                var patient = ListOfPatients();

                var jsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
                var jsonOfPatient = JsonConvert.SerializeObject(patient, Formatting.Indented, jsonSerializerSettings);
                return Content(jsonOfPatient, "application/json");


            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public List<PatientViewModel> ListOfPatients()
        {
            var dbpatients = db.Patients;
            var patients = new List<PatientViewModel>();
            string[] CaseStatus = { "OsteoporosisWithFracture", "Osteoporosis", "Osteopenia", "Normal" };

            foreach (var item in dbpatients)
            {
                patients.Add(new PatientViewModel
                {
                    Id = item.PatientId,
                    FullName = item.PatientFirstName + " " + item.PatientFatherName + " " + item.PatientLastName,
                    NationalId = item.NationalId,
                    OsteoporosisCase = CaseStatus[item.OsteoporosisCase],
                    ServiceCity = db.Cities.FirstOrDefault(c => c.CityId == item.ServiceCityId)?.CityName,
                    LiveCity = db.Cities.FirstOrDefault(c => c.CityId == item.LiveCityId)?.CityName,
                    isDeleted = item.isDeleted,
                    FileUrl = item.FileUrl
                });
            }

            var user = db.Users.FirstOrDefault(a => a.UserName == User.Identity.Name);
            var adminCity = db.Cities.FirstOrDefault(u => u.CityId == user.ServiceCityId)?.CityName;
            var patient = patients.Where(p => p.ServiceCity == adminCity);
            return patient.ToList();
        }


        private string CopyFile(string sourceDir, string sourcePath, string targetPath)
        {
            var baseFIle = sourcePath.Split('.')[0];
            sourcePath = System.Web.HttpContext.Current.Server.MapPath(sourcePath);
            var virtualTarget = targetPath;
            var fileName = Guid.NewGuid() + (new FileInfo(sourcePath).Extension);
            baseFIle = System.Web.HttpContext.Current.Server.MapPath(baseFIle);

            targetPath = System.Web.HttpContext.Current.Server.MapPath(targetPath);
            var destFile = Path.Combine(targetPath, fileName);

            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            try
            {
                System.IO.File.Copy(baseFIle, destFile, true);
            }
            catch (Exception ex)
            {
                // ignored
            }
            return virtualTarget + "/" + fileName;
        }
        void ClearAfterSave(string sourceDir)
        {
            var sourcePath = System.Web.HttpContext.Current.Server.MapPath(sourceDir);
            if (Directory.Exists(sourcePath))
            {
                var files = Directory.GetFiles(sourcePath);
                // Copy the files and overwrite destination files if they already exist.
                foreach (var s in files)
                {
                    System.IO.File.Delete(s);
                }
            }

        }

        [HttpPost]
        [ActionName("PostPatient")]
        public ActionResult PostPatient(Patient patient)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Bad Request");
            }

            patient.UserId = db.Users.FirstOrDefault(a => a.UserName == User.Identity.Name)?.Id;
            patient.ServiceCityId = db.Users.FirstOrDefault(a => a.UserName == User.Identity.Name).ServiceCityId;

            db.Patients.Add(patient);
            db.SaveChanges();
            var folderName = "~/SysFiles/PatientFiles/" + patient.PatientId.ToString();
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(Server.MapPath(folderName));
            }


            if (!string.IsNullOrEmpty(patient.FileUrl))
            {
                string[] files = patient.FileUrl.Split(',');
                patient.FileUrl = "";
                for (int i = 0; i < files.Length; i++)
                {
                    var newFilePath = CopyFile("~/SysFiles/Tmp", files[i], folderName);
                    patient.FileUrl += newFilePath + ((i == files.Length - 1) ? "" : ",");
                }
                ClearAfterSave("~/SysFiles/Tmp");
            }
            db.SaveChanges();

            return Json(new { id = patient.PatientId });
        }

        [HttpGet]
        [ActionName("GetPatientFiles")]
        public ActionResult GetPatientFiles(int id)
        {
            var patientFileUrl = db.Patients.FirstOrDefault(p => p.PatientId == id).FileUrl;
            if (patientFileUrl == null)
            {
                return Json(new { files = "Nofiles" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string[] files = patientFileUrl.Split(',');
                return Json(new { files = files }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}