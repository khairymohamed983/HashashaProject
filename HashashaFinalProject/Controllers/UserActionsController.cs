﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HashashaSPA.Models;

namespace HashashaSPA.Controllers
{
    public class UserActionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/UserActions
        public IQueryable<UserAction> GetUserActions()
        {
            return db.UserActions;
        }

        // GET: api/UserActions/5
        [ResponseType(typeof(UserAction))]
        public async Task<IHttpActionResult> GetUserAction(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            UserAction userAction = db.UserActions.FirstOrDefault(a => a.Id == id);

            var userName = db.Users.FirstOrDefault(u => u.Id == id).UserName;

            UserActionViewModel useract = new UserActionViewModel()
            {
                ActionDate = userAction.ActionDate,
                ActionDetails = userAction.ActionDetails,
                UserName = userName
            };
            if (userAction == null)
            {
                return NotFound();
            }

            return Ok(useract);
        }

        // PUT: api/UserActions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserAction(int id, UserAction userAction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userAction.ActionId)
            {
                return BadRequest();
            }

            db.Entry(userAction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserActionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserActions
        [ResponseType(typeof(UserAction))]
        public async Task<IHttpActionResult> PostUserAction(UserActionViewModel userActionVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UserAction userAction = new UserAction()
            {
                ActionDate = DateTime.Now,
                Id = userActionVM.Id,
                ActionDetails = userActionVM.ActionDetails
            };

            db.UserActions.Add(userAction);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = userAction.ActionId }, userAction);
        }

        // DELETE: api/UserActions/5
        [ResponseType(typeof(UserAction))]
        public async Task<IHttpActionResult> DeleteUserAction(int id)
        {
            UserAction userAction = db.UserActions.Find(id);
            if (userAction == null)
            {
                return NotFound();
            }

            db.UserActions.Remove(userAction);
            db.SaveChanges();

            return Ok(userAction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserActionExists(int id)
        {
            return db.UserActions.Count(e => e.ActionId == id) > 0;
        }
    }
}