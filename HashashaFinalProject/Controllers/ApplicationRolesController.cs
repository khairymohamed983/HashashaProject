﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HashashaSPA.Models;

namespace HashashaSPA.Controllers
{
    [RoutePrefix("api/ApplicationRoles")]
    public class ApplicationRolesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ApplicationRoles 
        public List<RoleViewModel> GetRoles()
        {
            db.Configuration.ProxyCreationEnabled = false;

            var dbRoles = db.Roles;
            var roles = new List<RoleViewModel>();
            foreach (var item in dbRoles)
            {
                var dbpriv = db.RolePrivilages.Where(p => p.Id == item.Id).ToList();
                List<RolePrivilageViewModel> rolepriv = new List<RolePrivilageViewModel>();
                foreach (var rol in dbpriv)
                {
                    var privName = db.Privilages.FirstOrDefault(a => a.PrivilageId == rol.PrivilageId)?.PrivilageName;
                    rolepriv.Add(new RolePrivilageViewModel { isAllowed = rol.isAllowed, PrivilageName = privName, PrivilageId = rol.PrivilageId });
                }
                roles.Add(new RoleViewModel
                {
                    RoleId = item.Id,
                    RoleName = item.Name,
                    RolePrivilages = rolepriv,
                    isDeleted = item.isDeleted
                });
            }
            return roles;
        }

        // GET: api/ApplicationRoles/5
        [ResponseType(typeof(ApplicationRole))]
        public IHttpActionResult GetApplicationRole(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            ApplicationRole applicationRole = db.Roles.Find(id);
            if (applicationRole == null)
            {
                return NotFound();
            }

            return Ok(applicationRole);
        }

        // PUT: api/ApplicationRoles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutApplicationRole(string id, EditRoleViewModel applicationRoleVm)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != applicationRoleVm.RoleId)
            {
                return BadRequest();
            }

            if (applicationRoleVm.isDeleted)
            {
                if (db.ApplicationUserRoles.Any(ar => ar.RoleId == applicationRoleVm.RoleId))
                {
                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.MethodNotAllowed));
                }

            }

            var role = db.Roles.FirstOrDefault(a => a.Id == id);
            role.isDeleted = applicationRoleVm.isDeleted;
            role.Name = applicationRoleVm.RoleName;

            foreach (var item in applicationRoleVm.RolePrivilages)
            {
                var dbrole = db.RolePrivilages.FirstOrDefault(p => p.PrivilageId == item.PrivilageId);
                if (dbrole != null) dbrole.isAllowed = item.isAllowed;
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationRoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApplicationRoles
        [ResponseType(typeof(ApplicationRole))]
        public IHttpActionResult PostApplicationRole(AddRoleViewModel roleVm)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = db.Roles.Any(a => a.Name == roleVm.RoleName);
            if (result)
            {
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
            }
            ApplicationRole role = new ApplicationRole()
            {
                Name = roleVm.RoleName,
                isDeleted = roleVm.isDeleted,
            };

            db.Roles.Add(role);
            db.SaveChanges();

            var roleId = db.Roles.FirstOrDefault(r => r.Name == roleVm.RoleName)?.Id;
            var lstRPriv = new List<RolePrivilage>();
            foreach (var item in roleVm.RolePrivilages)
            {
                lstRPriv.Add(new RolePrivilage { Id = roleId, PrivilageId = item.PrivilageId, isAllowed = item.isAllowed });
            }
            db.RolePrivilages.AddRange(lstRPriv);


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ApplicationRoleExists(role.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = role.Id }, roleVm);
        }

        // DELETE: api/ApplicationRoles/5
        [ResponseType(typeof(ApplicationRole))]
        public IHttpActionResult DeleteApplicationRole(string id)
        {
            ApplicationRole applicationRole = db.Roles.Find(id);
            if (applicationRole == null)
            {
                return NotFound();
            }
            applicationRole.isDeleted = !applicationRole.isDeleted;
            db.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        [Route("DeleteRole/{id}")]
        [ResponseType(typeof(ApplicationRole))]
        public IHttpActionResult DeleteRole(string id)
        {
            ApplicationRole applicationRole = db.Roles.Find(id);
            if (applicationRole == null)
            {
                return NotFound();
            }
            if (applicationRole.Users.Count > 0)
            {
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
            }
            db.Roles.Remove(applicationRole);
            db.SaveChanges();
            return Ok();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationRoleExists(string id)
        {
            return db.Roles.Count(e => e.Id == id) > 0;
        }
    }
}