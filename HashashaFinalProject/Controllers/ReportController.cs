﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HashashaSPA.Controllers
{
    public class ReportController : Controller
    {
        [HttpGet]
        [ActionName("PatientReport")]
        // GET: Report
        public ActionResult PatientReport()
        {
            ReportDocument doc = new ReportDocument();
            doc.Load(Server.MapPath("~/Reports/PatientReport.rpt"));
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            var strm = doc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
            strm.Seek(0, SeekOrigin.Begin);
            Response.AppendHeader("content-disposition", "inline;filename=PatientFIleTest" + ".doc");

            return File(strm, "application/msword");

        }
    }
}