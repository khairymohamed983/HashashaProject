﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HashashaSPA.Models;

namespace HashashaSPA.Controllers
{
    public class PrivilagesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Privilages
        public IQueryable<Privilage> GetPrivilages()
        {
            return db.Privilages;
        }

        // GET: api/Privilages/5
        [ResponseType(typeof(Privilage))]
        public async Task<IHttpActionResult> GetPrivilage(int id)
        {
            Privilage privilage = await db.Privilages.FindAsync(id);
            if (privilage == null)
            {
                return NotFound();
            }

            return Ok(privilage);
        }

        // PUT: api/Privilages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPrivilage(int id, Privilage privilage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != privilage.PrivilageId)
            {
                return BadRequest();
            }

            db.Entry(privilage).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PrivilageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Privilages
        [ResponseType(typeof(Privilage))]
        public async Task<IHttpActionResult> PostPrivilage(Privilage privilage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Privilages.Add(privilage);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = privilage.PrivilageId }, privilage);
        }

        // DELETE: api/Privilages/5
        [ResponseType(typeof(Privilage))]
        public async Task<IHttpActionResult> DeletePrivilage(int id)
        {
            Privilage privilage = await db.Privilages.FindAsync(id);
            if (privilage == null)
            {
                return NotFound();
            }

            db.Privilages.Remove(privilage);
            await db.SaveChangesAsync();

            return Ok(privilage);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PrivilageExists(int id)
        {
            return db.Privilages.Count(e => e.PrivilageId == id) > 0;
        }
    }
}