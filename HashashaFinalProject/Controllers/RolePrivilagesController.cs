﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HashashaSPA.Models;

namespace HashashaSPA.Controllers
{
    public class RolePrivilagesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/RolePrivilages
        public IQueryable<RolePrivilage> GetRolePrivilages()
        {
            return db.RolePrivilages;
        }

        // GET: api/RolePrivilages/5
        [ResponseType(typeof(RolePrivilage))]
        public async Task<IHttpActionResult> GetRolePrivilage(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<RolePrivilage> rolePrivilage = db.RolePrivilages.Where(a => a.Id == id).ToList();
            if (rolePrivilage == null)
            {
                return NotFound();
            }

            return Ok(rolePrivilage);
        }

        // PUT: api/RolePrivilages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRolePrivilage(string id, RolePrivilage rolePrivilage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rolePrivilage.Id)
            {
                return BadRequest();
            }

            db.Entry(rolePrivilage).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RolePrivilageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RolePrivilages
        [ResponseType(typeof(RolePrivilage))]
        public async Task<IHttpActionResult> PostRolePrivilage(RolePrivilage rolePrivilage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RolePrivilages.Add(rolePrivilage);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RolePrivilageExists(rolePrivilage.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = rolePrivilage.Id }, rolePrivilage);
        }

        // DELETE: api/RolePrivilages/5
        [ResponseType(typeof(RolePrivilage))]
        public async Task<IHttpActionResult> DeleteRolePrivilage(string id)
        {
            RolePrivilage rolePrivilage = await db.RolePrivilages.FindAsync(id);
            if (rolePrivilage == null)
            {
                return NotFound();
            }

            db.RolePrivilages.Remove(rolePrivilage);
            await db.SaveChangesAsync();

            return Ok(rolePrivilage);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RolePrivilageExists(string id)
        {
            return db.RolePrivilages.Count(e => e.Id == id) > 0;
        }
    }
}