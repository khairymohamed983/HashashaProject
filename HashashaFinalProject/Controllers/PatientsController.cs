﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using HashashaSPA.Models;
using System.Web;
using System.IO;
using System.Security.Claims;
using System.Threading;
using Microsoft.AspNet.Identity;

namespace HashashaSPA.Controllers
{

    public class PatientsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Patients
        public List<PatientViewModel> GetPatients()
        {
            db.Configuration.ProxyCreationEnabled = false;

            var dbpatients = db.Patients;
            var patients = new List<PatientViewModel>();
            string[] CaseStatus = { "OsteoporosisWithFracture", "Osteoporosis", "Osteopenia", "Normal" };

            foreach (var item in dbpatients)
            {
                patients.Add(new PatientViewModel
                {
                    Id = item.PatientId,
                    FullName = item.PatientFirstName + " " + item.PatientFatherName + " " + item.PatientLastName,
                    NationalId = item.NationalId,
                    OsteoporosisCase = CaseStatus[item.OsteoporosisCase],
                    ServiceCity = db.Cities.FirstOrDefault(c => c.CityId == item.ServiceCityId)?.CityName,
                    LiveCity = db.Cities.FirstOrDefault(c => c.CityId == item.LiveCityId)?.CityName,
                    isDeleted = item.isDeleted,
                    FileUrl = item.FileUrl
                });
            }

            return patients;
        }

        // GET: api/Patients/5
        [ResponseType(typeof(Patient))]
        public IHttpActionResult GetPatient(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Patient patient = db.Patients.Find(id);

            if (patient == null)
            {
                return NotFound();
            }
            return Ok(patient);
        }

        // PUT: api/Patients/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPatient(int id, Patient patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patient.PatientId)
            {
                return BadRequest();
            }
            var dbPatient = db.Patients.FirstOrDefault(a => a.PatientId == patient.PatientId);
            var dbfiles = dbPatient.FileUrl;
            var folderName = "~/SysFiles/PatientFiles/" + patient.PatientId.ToString();
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(folderName));
            }


            // remove last comma if found 
            var comma = patient.FileUrl[patient.FileUrl.Length - 1];
            if (comma == ',')
            {
                patient.FileUrl.Remove(patient.FileUrl.Length - 1);
            }

            var dbfilesarr = dbfiles.Split(',');
            if (!string.IsNullOrEmpty(patient.FileUrl))
            {
                string[] files = patient.FileUrl.Split(',');
                //dbfiles = "";
                for (int i = 0; i < files.Length; i++)
                {
                    var newFilePath = CopyFile("~/SysFiles/Tmp", files[i], folderName);
                    dbfiles += (dbfilesarr.Length >= 1 ? "," : "") + newFilePath + ((i == files.Length - 1) ? "" : ",");
                }
                ClearAfterSave("~/SysFiles/Tmp");
            }


            dbPatient.FileUrl = dbfiles;

            dbPatient.BirthDate = patient.BirthDate;
            dbPatient.BMDFemoral = patient.BMDFemoral;
            dbPatient.BMDTotalHip = patient.BMDTotalHip;
            dbPatient.BodyMassIndex = patient.BodyMassIndex;
            dbPatient.ContactNumber = patient.ContactNumber;
            dbPatient.CreationDate = patient.CreationDate;
            dbPatient.DXAFemoral = patient.DXAFemoral;
            dbPatient.DXALumber = patient.DXALumber;
            dbPatient.DXATotalHip = patient.DXATotalHip;
            dbPatient.Education = patient.Education;
            dbPatient.HasFracture = patient.HasFracture;
            dbPatient.Height = patient.Height;
            dbPatient.isDeleted = patient.isDeleted;
            dbPatient.LiveCityId = patient.LiveCityId;
            dbPatient.MobileNumber = patient.MobileNumber;
            dbPatient.NationalId = patient.NationalId;
            dbPatient.Occupation = patient.Occupation;
            dbPatient.OsteoporosisCase = patient.OsteoporosisCase;
            dbPatient.PatientFatherName = patient.PatientFatherName;
            dbPatient.PatientFirstName = patient.PatientFirstName;
            dbPatient.PatientGender = patient.PatientGender;
            dbPatient.PatientLastName = patient.PatientLastName;
            dbPatient.RiskFactor = patient.RiskFactor;
            dbPatient.ServiceCityId = patient.ServiceCityId;
            dbPatient.Weight = patient.Weight;


            db.Entry(dbPatient).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PatientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        private string CopyFile(string sourceDir, string sourcePath, string targetPath)
        {
            var baseFIle = sourcePath.Split('.')[0];
            sourcePath = HttpContext.Current.Server.MapPath(sourcePath);
            var virtualTarget = targetPath;
            var fileName = Guid.NewGuid() + (new FileInfo(sourcePath).Extension);
            baseFIle = HttpContext.Current.Server.MapPath(baseFIle);

            targetPath = HttpContext.Current.Server.MapPath(targetPath);
            var destFile = Path.Combine(targetPath, fileName);

            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            try
            {
                File.Copy(baseFIle, destFile, true);
            }
            catch (Exception ex)
            {
                // ignored
            }
            return virtualTarget + "/" + fileName;
        }
        void ClearAfterSave(string sourceDir)
        {
            var sourcePath = HttpContext.Current.Server.MapPath(sourceDir);
            if (Directory.Exists(sourcePath))
            {
                var files = Directory.GetFiles(sourcePath);
                // Copy the files and overwrite destination files if they already exist.
                foreach (var s in files)
                {
                    File.Delete(s);
                }
            }

        }

        // POST: api/Patients
        //[ResponseType(typeof(Patient))]
        //public IHttpActionResult PostPatient(Patient patient)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (!string.IsNullOrEmpty(patient.FileUrl))
        //    {
        //        var newFilePath = CopyFile("~/SysFiles/Tmp", patient.FileUrl, "~/SysFiles/Files");
        //        patient.FileUrl = newFilePath;
        //        ClearAfterSave("~/SysFiles/Tmp");
        //    }

        //    db.Patients.Add(patient);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = patient.PatientId }, patient);
        //}

        // DELETE: api/Patients/5
        [ResponseType(typeof(Patient))]
        public IHttpActionResult DeletePatient(int id)
        {
            Patient patient = db.Patients.Find(id);
            if (patient == null)
            {
                return NotFound();
            }

            db.Patients.Remove(patient);
            db.SaveChanges();

            return Ok(patient);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PatientExists(int id)
        {
            return db.Patients.Count(e => e.PatientId == id) > 0;
        }
    }
}