﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HashashaSPA.Models;

namespace HashashaSPA.Controllers
{
    [RoutePrefix("api/ApplicationUsers")]
    public class ApplicationUsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ApplicationUsers/GetApplicationUsers 
        [Route("GetApplicationUsers")]
        public List<UserViewModel> GetApplicationUsers()
        {
            //db.Configuration.ProxyCreationEnabled = false;
            var dbusers = db.Users;
            var users = new List<UserViewModel>();

            foreach (var item in dbusers)
            {
                //var userroles = db.ApplicationUserRoles;
                var role = db.ApplicationUserRoles.FirstOrDefault(a => a.UserId == item.Id);
                users.Add(new UserViewModel
                {
                    Id = item.Id,
                    isDeleted = item.isDeleted,
                    FullName = item.FullName,
                    UserName = item.UserName,
                    RoleId = role.RoleId,
                    RoleName = db.Roles.FirstOrDefault(a => a.Id == role.RoleId).Name,
                    CreateDate = item.CreationDate,
                    LastLogin = item.LastLogin,
                   // HealthFacility = item.HealthFacility,
                    HealthFacility = item.SecurityStamp,
                    JobTitle = item.JobTitle,
                    CityId = item.ServiceCityId,
                    MobileNumber = item.PhoneNumber,
                    Email = item.Email
                });
            }
            return users;
        }
        // GET: api/Users/5
        [ResponseType(typeof(ApplicationUser))]
        public UserViewModel GetApplicationUser(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            ApplicationUser applicationUser = db.Users.Find(id);

            var role = db.ApplicationUserRoles.FirstOrDefault(a => a.UserId == applicationUser.Id);
            var dbpriv = db.UserPrivilages.Where(p => p.Id == id).ToList();
            List<UserViewPrivilage> userpriv = new List<UserViewPrivilage>();
            foreach (var item in dbpriv)
            {
                userpriv.Add(new UserViewPrivilage { isAllowed = item.isAllowed, PrivilageId = item.PrivilageId });
            }
            var user = new UserViewModel()
            {
                Id = applicationUser.Id,
                UserName = applicationUser.UserName,
                FullName = applicationUser.FullName,
                CreateDate = applicationUser.CreationDate,
                LastLogin = applicationUser.LastLogin,
                RoleId = role.RoleId,
                RoleName = db.Roles.FirstOrDefault(r => r.Id == role.RoleId).Name,
                isDeleted = applicationUser.isDeleted,
                UserPrivilages = userpriv,
                //HealthFacility = applicationUser.HealthFacility,
                HealthFacility = applicationUser.SecurityStamp,
                JobTitle = applicationUser.JobTitle,
                CityId = applicationUser.ServiceCityId,
                MobileNumber = applicationUser.PhoneNumber,
                Email = applicationUser.Email
            };
            return user;
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutApplicationUser(string id, EditUserViewModel applicationUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != applicationUser.Id)
            {
                return BadRequest();
            }

            // if user create any patint prevent delete action
            if (applicationUser.isDeleted)
            {
                if (db.Patients.Any(p => p.UserId == id))
                {
                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.MethodNotAllowed));
                }
            }

            var user = db.Users.FirstOrDefault(a => a.Id == id);
            user.isDeleted = applicationUser.isDeleted;
            user.UserName = applicationUser.UserName;
            user.FullName = applicationUser.FullName;
            user.ServiceCityId = applicationUser.CityId;
            user.Email = applicationUser.Email;
            user.PhoneNumber = applicationUser.MobileNumber;
            user.JobTitle = applicationUser.JobTitle;
            //user.HealthFacility = applicationUser.HealthFacility;
            user.SecurityStamp = applicationUser.HealthFacility;

            var role = db.ApplicationUserRoles.FirstOrDefault(a => a.UserId == applicationUser.Id);
            if (role == null)
            {
                db.ApplicationUserRoles.Add(new ApplicationUserRole { RoleId = applicationUser.RoleId, UserId = applicationUser.Id });
            }
            else if (role.RoleId != applicationUser.RoleId)
            {
                db.ApplicationUserRoles.Remove(role);
                db.ApplicationUserRoles.Add(new ApplicationUserRole { RoleId = applicationUser.RoleId, UserId = applicationUser.Id });
            }
            var userpriv = db.UserPrivilages.Where(a => a.Id == applicationUser.Id);
            foreach (var item in userpriv)
            {
                item.isAllowed = applicationUser.UserPrivilages.FirstOrDefault(a => a.PrivilageId == item.PrivilageId).isAllowed;
            }

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(ApplicationUser))]
        public async Task<IHttpActionResult> PostApplicationUser(ApplicationUser applicationUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(applicationUser);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ApplicationUserExists(applicationUser.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = applicationUser.Id }, applicationUser);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(ApplicationUser))]
        //[HashAuthorize(PrivilageId = 9)]
        public async Task<IHttpActionResult> DeleteApplicationUser(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            db.Users.Remove(applicationUser);
            await db.SaveChangesAsync();

            return Ok(applicationUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationUserExists(string id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}