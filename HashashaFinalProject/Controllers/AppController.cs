﻿using System.Web.Mvc;

namespace HashashaSPA.Controllers
{
    //[HashAuthorize]
    public class AppController : Controller
    {
        public ActionResult home()
        {
            return PartialView();
        }
        public ActionResult addForm()
        {
            return PartialView();
        }

        public ActionResult allForms()
        {
            return PartialView();
        }


        public ActionResult preview()
        {
            return PartialView();
        }

        public ActionResult editPatient()
        {
            return PartialView();
        }

        public ActionResult cities()
        {
            return PartialView();
        }

        public ActionResult users()
        {
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult login()
        {
            return PartialView();
        }

        public ActionResult registerUser()
        {
            return PartialView();
        }

        public ActionResult roles()
        {
            return PartialView();
        }

        public ActionResult filterPatient()
        {
            return PartialView();
        }

        public ActionResult editProfile()
        {
            return PartialView();
        }

        public ActionResult map()
        {
            return PartialView();
        }
    }

}