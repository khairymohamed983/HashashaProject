﻿using HashashaSPA.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HashashaSPA.Controllers
{
    [RoutePrefix("api/Check")]
    public class CheckController : ApiController
    {
        ApplicationDbContext db = new ApplicationDbContext();
        [HttpGet]
        [Route("CheckDelete")]
        public async Task<HttpResponseMessage> CheckDelete(int key)
        {
            var allPatient = db.Patients.Any(p => p.ServiceCityId == key || p.LiveCityId == key);
            List<string> errors = new List<string>() { };
            if (allPatient)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
                //var deletedPatient = db.Patients.Where(p => p.ServiceCityId == key);
                //db.Patients.RemoveRange(deletedPatient);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

      
    }
}
