﻿using System.Web.Optimization;

namespace HashashaSPA
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/jq").Include(
            "~/Content/plugins/jQuery/jQuery-2.1.4.min.js",
            //"~/Scripts/jquery.min.js",
             "~/Scripts/jquery-ui.min.js",
             "~/Content/bootstrap/js/bootstrap.min.js",
             "~/Scripts/raphael-min.js",
             "~/Content/plugins/morris/morris.min.js",
             "~/Content/plugins/sparkline/jquery.sparkline.min.js",
             "~/Content/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
             "~/Content/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
             "~/Content/plugins/knob/jquery.knob.js",
             "~/Content/plugins/slimScroll/jquery.slimscroll.min.js",
             "~/Content/plugins/fastclick/fastclick.min.js",
             "~/Content/dist/js/app.min.js"
             ));

            bundles.Add(new ScriptBundle("~/ng").Include(
            "~/Scripts/angular-file-upload-shim.js",
            "~/Scripts/angular.min.js",
            "~/Scripts/angular-file-upload.js",
            "~/Scripts/angular-local-storage.min.js",
            "~/Scripts/angular-messages.min.js",
            "~/Scripts/angular-route.min.js",
            "~/Scripts/angular-animate.min.js",
            "~/Scripts/angular-cookies.min.js",
            "~/Scripts/angular-sanitize.min.js",
            "~/Scripts/moment.min.js",
            "~/Scripts/angular-moment.min.js", 
            "~/Scripts/angular-ui/ui-bootstrap-tpls.min.js",
            "~/Scripts/dirPagination.js",
            "~/Scripts/angular-spinners.js",
            "~/Scripts/toaster.js",
            "~/Scripts/odataresources.min.js",
            "~/Scripts/app/app.js",
            "~/Scripts/app/angular.audio.js",
            "~/Scripts/select.js",
            "~/Scripts/angular-translate.min.js",
            "~/Scripts/angular-translate-loader-static-files.min.js",
            "~/Scripts/angular-validation.min.js",
            "~/Scripts/name_generator.js",
            "~/Scripts/egyptian_set.js",
            "~/Scripts/fileOperations.js"
            ));
        }
    }
}
