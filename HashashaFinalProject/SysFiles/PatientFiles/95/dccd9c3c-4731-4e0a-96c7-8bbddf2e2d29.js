angular.module('subcategoriesmodule', [
    'ngAnimate',
    'ngGrid',
    'SignalrDataModule'
]).controller('subcategoriesController', ['$scope', '$filter', '$http', '$q', 'SignalrDataFactory', function ($scope, $filter, $http, $q, SignalrDataFactory) {
    //'use strict';
    //Constants 
    var HubFunctionname = 'productsubcategory';
    var GroupName = 'ProductSubCategoryGroup';
    var ApisLinks = [
        'SubCategory/GetAllSubCategories',
        'SubCategory/GetSingleSubCategory/',
        'SubCategory/PostSubCategory',
        'SubCategory/PutSubCategory',
        'SubCategory/DeleteSubCategory/',
    ];

    SignalrDataFactory.InitateConnection();

    SignalrDataFactory.InvokeClientMethod('clientpost' + HubFunctionname + 'callback', function (msg) {
        $scope.MainData.push(msg);
        $scope.setPagingData($scope.MainData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
    });

    SignalrDataFactory.InvokeClientMethod('clientdelete' + HubFunctionname + 'callback', function (id) {

        $scope.MainData = $scope.MainData.filter(function (obj) {
            /**********CHECKLOGIC*********/
            return obj.SubCategoryId !== id;
        });
        $scope.setPagingData($scope.MainData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
    });

    SignalrDataFactory.InvokeClientMethod('clientput' + HubFunctionname + 'callback', function (msg) {
        var index = -1
        for (var i = 0; i < $scope.MainData.length; i++) {
            /**********CHECKLOGIC*********/
            if ($scope.MainData[i].SubCategoryId == msg.SubCategoryId) {
                $scope.MainData[i].SubCategoryName = msg.SubCategoryName;
                $scope.MainData[i].Category = msg.Category;
                $scope.MainData[i].IsNeglected = false;
                break;
            }
        }
        $scope.setPagingData($scope.MainData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);

    });
    //-----------------------------
    //Constants (Ng-Hide)
    $scope.isPosting = true;
    $scope.isEditing = true;
    $scope.showDiv = true;
    $scope.DismissForm = function () {
        $scope.isPosting = true;
        $scope.isEditing = true;
        $scope.showDiv = true;
    }
    $scope.showAddForm = function () {
        //Clicked by the Add button
        $scope.isPosting = false;
        $scope.isEditing = true;
        $scope.showDiv = false;

    }


    $scope.PostedObj = {};


    $scope.postObject = function () {
        /**********CHECKLOGIC*********/
        var Obj = {
            SubCategoryId: null,
            SubCategoryName: $scope.PostedObj.SubCategoryName,
            CategoryId: $scope.parentCategoryPost.PostedObj.CategoryId,
            IsNeglected: false,
            CreateDate: null
        };
        SignalrDataFactory.Post(ApisLinks[2], Obj);
        $scope.isPosting = true;
        $scope.isEditing = true;
        $scope.showDiv = true;
        $scope.PostedObj = {};

    };


    //Deleteing all seleceted cities 
    $scope.deleteObject = function () {
        for (obj in $scope.gridOptions.selectedItems) {
            /**********CHECKLOGIC*********/
            SignalrDataFactory.Delete(ApisLinks[4], $scope.gridOptions.selectedItems[obj].SubCategoryId);
        }
        $scope.gridOptions.$gridScope.toggleSelectAll(false, false);
    };




    $scope.EditObj = {};
    //Load Edit
    $scope.loadEditData = function () {
        $scope.isAdding = false;
        $scope.isEdtiing = true;
        var firstSelectedObj = $scope.gridOptions.selectedItems[0];
        var second = $scope.gridOptions.selectedItems;
        SignalrDataFactory.GetSingle(ApisLinks[1], firstSelectedObj.SubCategoryId).then(function (result) {
            //TODO : Fix the Edit Selecetion 
            $scope.EditObj = result.data;
           
            $scope.showDiv = false;
            $scope.isPosting = true;
            $scope.isEditing = false;
        });
    }
    //Editing Send
    $scope.PutEditObj = function () {
        /**********CHECKLOGIC*********/
        var PutedObject = {
            SubCategoryId: $scope.EditObj.SubCategoryId,
            SubCategoryName: $scope.EditObj.SubCategoryName,
            CategoryId: $scope.parentCategoryEdit.EditedObj.CategoryId,
            IsNeglected: false

        };
        console.log($scope.parentCategoryEdit.EditedObj);
        //SignalrDataFactory.InvokeServerMethod('lockproductcategory', GroupName, PutedObject.SubCategoryId);

        SignalrDataFactory.Put(ApisLinks[3], PutedObject).then(function () {
            //SignalrDataFactory.InvokeServerMethod('unlockproductcategory', GroupName, PutedObject.SubCategoryId);
            console.log("Edited Successfuly");
        });
        $scope.isPosting = true;
        $scope.isEditing = true;
        $scope.showDiv = true;
    }
    //------------------------Getting Main Catgories Data
    $scope.parentCategoryPost = {};
    $scope.parentCategoryEdit = {};
    $scope.categories = [];
    var getCategories = SignalrDataFactory.GetAll('Categories/GetAllCategories').then(function (result) {
        $scope.categories = result.data;
        $scope.parentCategoryPost.PostedObj = result.data[0];
        $scope.parentCategoryEdit.EditedObj = result.data[0];

    });






    //-----------------------------------------------------------------
    //----------------------- NG-GRID CONFIGURATION -------------------
    //$scope.MainData is the Array containing the elements in the grid-
    $scope.MainData = [];
    $scope.filterOptions = {
        filterText: '',
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [25, 50, 100],
        pageSize: 25,
        currentPage: 1
    };
    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();

                var getAllPromise = SignalrDataFactory.GetAll(ApisLinks[0]).then(function (result) {
                    $scope.MainData = result.data.filter(function (item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) !== -1;
                    });
                    $scope.setPagingData($scope.MainData, page, pageSize);

                });
            } else {

                var getAllPromise = SignalrDataFactory.GetAll(ApisLinks[0]).then(function (result) {
                    $scope.MainData = result.data;
                    $scope.setPagingData($scope.MainData, page, pageSize);
                    console.log(result.data);


                });


            }
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: [],
        columnDefs: [
            { displayName: '#', cellTemplate: '<div>{{$parent.$index + 1}}</div>' },
            { field: 'SubCategoryName', displayName: 'Name' },
            { field: 'CategoryName  ', displayName: 'Parent' },
            { field: 'CreateDate', displayName: 'Date', cellFilter: 'date:"yyyy-MM-dd"' }



        ]

    };
    //-----------------------------------------------------------------
    //----------------------- NG-GRID CONFIGURATION -------------------
    //-----------------------------------------------------------------

}]);