﻿using HashashaSPA.Models;
using PostSharp.Aspects;
using System;

namespace HashashaSPA.Helpers
{
    [Serializable]
    public class LogAttribute : OnMethodBoundaryAspect
    {
        public string ActionDetails { get; set; }

        public override void OnExit(MethodExecutionArgs args)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var useraction = new UserAction()
            {
                ActionDate = DateTime.Now,
                ActionDetails = this.ActionDetails,
            };
            db.UserActions.Add(useraction);
            db.SaveChanges();
            base.OnExit(args);
        }

    }

}