﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

public static class ExtensionClass
{
    static string[] monthArray = { "يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر" };
    public static string GetMonthName(this int monthNumber)
    {
        return monthArray[monthNumber - 1];

    }
    public static string FormateDate(this string datestr)
    {
        // 12/12/2012 12:00:00 AM     في تمام
        string[] arr = datestr.Split(' ');
        string stem = "في يوم ";
        string time = "في تمام ";
        return string.Format("{0} {1} {2} {3} {4}", stem, arr[0], time, arr[1], arr[2] == "AM" ? "صباحاَ" : "مساءَ");
    }
    public static byte[] ReadToEnd(this System.IO.Stream stream)
    {
        long originalPosition = 0;

        if (stream.CanSeek)
        {
            originalPosition = stream.Position;
            stream.Position = 0;
        }

        try
        {
            byte[] readBuffer = new byte[4096];

            int totalBytesRead = 0;
            int bytesRead;

            while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
            {
                totalBytesRead += bytesRead;

                if (totalBytesRead == readBuffer.Length)
                {
                    int nextByte = stream.ReadByte();
                    if (nextByte != -1)
                    {
                        byte[] temp = new byte[readBuffer.Length * 2];
                        Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                        Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                        readBuffer = temp;
                        totalBytesRead++;
                    }
                }
            }

            byte[] buffer = readBuffer;
            if (readBuffer.Length != totalBytesRead)
            {
                buffer = new byte[totalBytesRead];
                Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
            }
            return buffer;
        }
        finally
        {
            if (stream.CanSeek)
            {
                stream.Position = originalPosition;
            }
        }
    }

    public static DateTime GetDate(this string stringDate, char sperator)
    {
        string[] dateArr = stringDate.Split(sperator);
        if (dateArr.Length != 3)
        {
            return DateTime.MinValue;
        }
        return new DateTime(int.Parse(dateArr[2]), int.Parse(dateArr[1]), int.Parse(dateArr[0]));
    }
    public static string RemoveMoneyZero(this string money)
    {
        if (money.Contains("."))
        {
            money = money.TrimEnd('0');
            if (money.IndexOf('.') == money.Length - 1)
                money = money.Replace(".", "");
            return money;
        }
        else
            return money;
    }
    public static string GetDateString(this DateTime date)
    {
        return date.Day + "/" + date.Month + "/" + date.Year;
    }

    public static string SaveImage(this HttpPostedFileBase image, string prefix)
    {
        string file = "";
        if (image != null && image.ContentLength > 0)
        {
            string path = string.IsNullOrEmpty(prefix) ? "~/Files" : "~/Files/" + prefix;
            string imagePath = Path.Combine(HttpContext.Current.Server.MapPath(path), image.FileName);
            string imageUrl = path + "/" + image.FileName;
            image.SaveAs(imagePath);
            file = imageUrl;
        }
        return file;
    }

    //public static bool IsValid(this string value, ValidationEnum expression)
    //{
    //    Regex myRegex=null;
    //    if (expression == ValidationEnum.Url)
    //        myRegex = new Regex(@"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?", RegexOptions.Compiled);
    //    else if(expression==ValidationEnum.Email)
    //        myRegex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled);
    //    return myRegex.IsMatch(value);    
    //}


    public static string FromDictionaryToJson(this Dictionary<string, int> dictionary)
    {
        var kvs = dictionary.Select(kvp => $"\"{kvp.Key}\":\"{string.Concat(kvp.Value)}\"");
        return string.Concat("{", string.Join(",", kvs), "}");
    }

    public static string FromDictionaryToJson(this Dictionary<string, List<int>> dictionary)
    {
        var kvs = dictionary.Select(kvp => $"\"{kvp.Key}\":\"{string.Concat(kvp.Value)}\"");
        return string.Concat("{", string.Join(",", kvs), "}");
    }

}
