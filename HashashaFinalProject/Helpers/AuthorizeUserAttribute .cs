﻿using HashashaSPA.Models;
using PostSharp.Aspects;
using System;
using System.Linq;
using System.Web;

[Serializable]
public class Autherization : OnMethodBoundaryAspect
{
    /// <summary>
    /// An Aspect By Khairy Mohammed to deliver the Authorization Procesess to the Method using Methodboundary aspect .
    /// </summary>
    /// <summary>
    /// Provide us The Type Of Action User Want To Apply On the Data .
    /// 0-> Create , 1-> Read , 2 -> Update,3-> Delete , 4-> Custom 
    /// </summary>
    public int PrivilageID { get; set; }
    /// <summary>
    /// Provide the Name of The Page User Do Actions On it .
    /// </summary> 
    public override void OnEntry(MethodExecutionArgs args)
    {
        ApplicationDbContext db = new ApplicationDbContext();

       
        string name = HttpContext.Current.Session["name"].ToString();

        var user = db.Users.Where(a => a.UserName == name).FirstOrDefault();
        var userId = db.Users.FirstOrDefault(p => p.UserName == HttpContext.Current.User.Identity.Name).Id;
        if (db.UserPrivilages.FirstOrDefault(a => a.Id == userId && a.PrivilageId == PrivilageID).isAllowed)
            OnExit(args);
    }
    public override void OnExit(MethodExecutionArgs args)
    {
        // Exit if User Not Autherize
        return;
    }
}


//[AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
//public class HashAuthorize : AuthorizeAttribute
//{

//    //Custom named parameters for annotation
//    public int PrivilageId { get; set; }

//    private ApplicationDbContext db = new ApplicationDbContext();


//    public override void OnAuthorization(HttpActionContext actionContext)
//    {
//        if (Authorize(actionContext))
//        {
//            return;
//        }
//        HandleUnauthorizedRequest(actionContext);
//    }

//    protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
//    {
//        var response = actionContext.Request.CreateResponse(HttpStatusCode.Moved);
//        response.Headers.Location = new Uri("http://www.google.com");
//        //actionContext.Response = new RedirectResult(new Uri(""),);
//        actionContext.Response = response;
//            //new HttpResponseMessage(HttpStatusCode.Unauthorized);
//        //var challengeMessage = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
//        //challengeMessage.Headers.Add("WWW-Authenticate", "Basic");
//        //throw new HttpResponseException(challengeMessage);
//    }

//    private bool Authorize(System.Web.Http.Controllers.HttpActionContext actionContext)
//    {
//        try
//        {
//            if (PrivilageId == -1)
//            {         
//                return true;
//            }
//            var userId = db.Users.FirstOrDefault(p => p.UserName == HttpContext.Current.User.Identity.Name).Id;
//            return db.UserPrivilages.FirstOrDefault(a => a.Id == userId && a.PrivilageId == PrivilageId).isAllowed;
//        }
//        catch (Exception)
//        {
//            return false;
//        }
//    }
//}

//Called when access is denied
//protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
//{
//    //User isn't logged in
//    if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
//    {
//        filterContext.Result = new RedirectToRouteResult(
//                new System.Web.Routing.RouteValueDictionary(new { controller = "Account", action = "Login" })
//        );
//    }
//    //User is logged in but has no access
//    else
//    {
//        filterContext.Result = new RedirectToRouteResult(
//                new System.Web.Routing.RouteValueDictionary(new { controller = "Account", action = "NotAuthorized" })
//        );
//    }
//}

//Core authentication, called before each action
//protected override bool AuthorizeCore(HttpContextBase httpContext)
//{
//    var isAuthorized = base.AuthorizeCore(httpContext);
//    if (!isAuthorized)
//    {
//        return false;
//    }
//    if (PrivilageId == -1)
//    {
//        return true;
//    }
// return db.UserPrivilages.FirstOrDefault(a => a.Id == db.Users.FirstOrDefault(p => p.UserName == httpContext.User.Identity.Name).Id && a.PrivilageId == PrivilageId).isAllowed;
//var b = httpContext.User.Identity.IsAuthenticated;
//Is user logged in?
//if (b)
//    //If user is logged in and we need a custom check:
//    if (ResourceKey != null && OperationKey != null)
//        return ecMembership.Instance.Member().ActivePermissions.Where(x => x.operation == OperationKey && x.resource == ResourceKey).Count() > 0;
////Returns true or false, meaning allow or deny. False will call HandleUnauthorizedRequest above
//return b;
//}
//}
