using System;
using System.Collections.Generic;
using HashashaSPA.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HashashaSPA.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HashashaSPA.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(HashashaSPA.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //context.Privilages.AddOrUpdate(a=>a.PrivilageId,
            //    new Privilage { PrivilageId = 1, PrivilageName = "ViewPatient" },
            //    new Privilage { PrivilageId = 2, PrivilageName = "CreatePatient" },
            //    new Privilage { PrivilageId = 3, PrivilageName = "EditPatient" },
            //    new Privilage { PrivilageId = 4, PrivilageName = "DeletePatient" },
            //    new Privilage { PrivilageId = 5, PrivilageName = "PrintReport" },
            //    new Privilage { PrivilageId = 6, PrivilageName = "ViewUsers" },
            //    new Privilage { PrivilageId = 7, PrivilageName = "CreateUser" },
            //    new Privilage { PrivilageId = 8, PrivilageName = "EditUser" },
            //    new Privilage { PrivilageId = 9, PrivilageName = "DeleteUser" },
            //    new Privilage { PrivilageId = 10, PrivilageName = "ViewRoles" },
            //    new Privilage { PrivilageId = 11, PrivilageName = "CreateRole" },
            //    new Privilage { PrivilageId = 12, PrivilageName = "EditRole" },
            //    new Privilage { PrivilageId = 13, PrivilageName = "DeleteRole" },
            //    new Privilage { PrivilageId = 14, PrivilageName = "ViewCities" },
            //    new Privilage { PrivilageId = 15, PrivilageName = "CreateCity" },
            //    new Privilage { PrivilageId = 16, PrivilageName = "EditCity" },
            //    new Privilage { PrivilageId = 17, PrivilageName = "DeleteCity" },
            //    new Privilage { PrivilageId = 18, PrivilageName = "DownloadPatient" },
            //    new Privilage { PrivilageId = 19, PrivilageName = "Reports" }
            //    );
            IList<Privilage> priv = new List<Privilage>();
            priv.Add(new Privilage { PrivilageId = 1, PrivilageName = "ViewPatient" });
            priv.Add(new Privilage { PrivilageId = 2, PrivilageName = "CreatePatient" });
            priv.Add(new Privilage { PrivilageId = 3, PrivilageName = "EditPatient" });
            priv.Add(new Privilage { PrivilageId = 4, PrivilageName = "DeletePatient" });
            priv.Add(new Privilage { PrivilageId = 5, PrivilageName = "PrintReport" });
            priv.Add(new Privilage { PrivilageId = 6, PrivilageName = "ViewUsers" });
            priv.Add(new Privilage { PrivilageId = 7, PrivilageName = "CreateUser" });
            priv.Add(new Privilage { PrivilageId = 8, PrivilageName = "EditUser" });
            priv.Add(new Privilage { PrivilageId = 9, PrivilageName = "DeleteUser" });
            priv.Add(new Privilage { PrivilageId = 10, PrivilageName = "ViewRoles" });
            priv.Add(new Privilage { PrivilageId = 11, PrivilageName = "CreateRole" });
            priv.Add(new Privilage { PrivilageId = 12, PrivilageName = "EditRole" });
            priv.Add(new Privilage { PrivilageId = 13, PrivilageName = "DeleteRole" });
            priv.Add(new Privilage { PrivilageId = 14, PrivilageName = "ViewCities" });
            priv.Add(new Privilage { PrivilageId = 15, PrivilageName = "CreateCity" });
            priv.Add(new Privilage { PrivilageId = 16, PrivilageName = "EditCity" });
            priv.Add(new Privilage { PrivilageId = 17, PrivilageName = "DeleteCity" });
            priv.Add(new Privilage { PrivilageId = 18, PrivilageName = "DownloadPatient" });
            priv.Add(new Privilage { PrivilageId = 19, PrivilageName = "Reports" });


            IList<UserPrivilage> uplst = new List<UserPrivilage>();
            foreach (Privilage p in priv)
            {
                UserPrivilage up = new UserPrivilage { isAllowed = true, PrivilageId = p.PrivilageId };
                if (!context.Privilages.Any())
                    context.Privilages.AddOrUpdate(p);

                uplst.Add(up);
            }


            // The UserStore is ASP Identity's data layer. Wrap context with the UserStore.
            UserStore<ApplicationUser> userStore = new UserStore<ApplicationUser>(context);

            //The UserManager is ASP Identity's implementation layer: contains the methods.
            //The constructor takes the UserStore: how the methods will interact with the database.
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(userStore);

            //Add or Update the initial Users into the database as normal.
            context.Users.AddOrUpdate(
                x => x.Email,  //Using Email as the Unique Key: If a record exists with the same email, AddOrUpdate skips it.
                new ApplicationUser
                {
                    Email = "admin@gmail.com",
                    EmailConfirmed = false,
                    UserName = "admin",
                    PasswordHash = new PasswordHasher().HashPassword("Admin@123"),
                    FullName = "Admin",
                    CreationDate = DateTime.Now,
                    LastLogin = DateTime.Now,
                    isDeleted = false,
                    PhoneNumber = "0101010101",
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                    JobTitle = "System Admin",
                    HealthFacility = 50,
                    ServiceCityId = 1
                });

            //Save changes so the Id columns will auto-populate.
            context.SaveChanges();

            //ASP Identity User Id's are Guids stored as nvarchar(128), and exposed as strings.

            //Get the UserId only if the SecurityStamp is not set yet.
            string userId = context.Users.Where(x => x.Email == "admin@gmail.com" && string.IsNullOrEmpty(x.SecurityStamp)).Select(x => x.Id).FirstOrDefault();

            //If the userId is not null, then the SecurityStamp needs updating.
            if (!string.IsNullOrEmpty(userId)) userManager.UpdateSecurityStamp(userId);

            //Continue on with Seed.
            
            //context.ApplicationUserRoles.AddOrUpdate(new ApplicationUserRole { RoleId = new Guid().ToString(), UserId = userId });
            //var lstPri = new List<UserPrivilage>();
            //foreach (var item in context.UserPrivilages)
            //{
            //    lstPri.Add(new UserPrivilage { Id = userId, PrivilageId = item.PrivilageId, isAllowed = true });
            //}
            //context.UserPrivilages.AddRange(lstPri);
            
            //context.SaveChanges();
        }
    }
}
