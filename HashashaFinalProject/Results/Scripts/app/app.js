﻿/// <reference path="../angular.min.js" />
var app = angular.module('app',
    ['ngAnimate', 'ngRoute', 'ngSanitize',
    'angularFileUpload', 'angularSpinners', 'ui.bootstrap', 'ui.select', 'ODataResources', 'toaster', 'ngMessages',
    'angularUtils.directives.dirPagination', 'pascalprecht.translate', 'angular.morris-chart', 'angularMoment', 'LocalStorageModule', 'ngCookies']);


app.config(['$provide', '$routeProvider', '$httpProvider', '$locationProvider',
    function ($provide, $routeProvider, $httpProvider, $locationProvider) {
        $provide.decorator('$templateRequest', ['$delegate', function ($delegate) {
            var mySilentProvider = function (tpl, ignoreRequestError) {
                return $delegate(tpl, true);
            }
            return mySilentProvider;
        }]);

        //$httpProvider.interceptors.push(['$q', '$location', '$rootScope', function ($q, $location, $rootScope) {
        //    return {
        //        'request': function (config) {
        //            //console.log('requesting resource');
        //            $rootScope.showWait();
        //            return config;
        //        },
        //        'requestError': function (rejection) {
        //            $rootScope.hideWait();
        //            return $q.reject('Could not Resolve the Request .');
        //        },
        //        'response': function (response) {
        //            // console.log(' resource comming ..');
        //            $rootScope.hideWait();
        //            return response;
        //        },
        //        'responseError': function (response) {
        //            $rootScope.hideWait();
        //            if (response.status === 401) {

        //            }
        //            return $q.reject(response);
        //        }
        //    };
        //}]);

        $routeProvider.when('/', {
            templateUrl: 'App/home',
            controller: 'homeCtrl'
        });
        var view = '';
        $routeProvider.when('/:name*', {
            templateUrl: function (urlattr) {
                view = urlattr.name;
                return '/App/' + urlattr.name;
            }
        });
        $routeProvider.otherwise({
            templateUrl: '/'
        });
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });


    }]);

app.run(['$rootScope', '$timeout', 'spinnerService',
    function ($rootScope, $timeout, spinnerService) {
        $rootScope.showWait = function () {
            $rootScope.isLoading = true;
            spinnerService.show('html5spinner');
        };
        $rootScope.hideWait = function () {
            $timeout(function () {
                spinnerService.hide('html5spinner');
            }, 700);
            $rootScope.isLoading = false;
        };
    }]);

app.run(['$rootScope', 'toaster', function ($rootScope, toaster) {
    $rootScope.showSuccess = function (title, body) {
        toaster.pop('success', title, body, 15000, 'trustedHtml');
    };

    $rootScope.showError = function (title, body) {
        toaster.pop('error', title, body, 15000, 'trustedHtml');
    };

    $rootScope.showWarning = function (title, body) {
        toaster.pop('warning', title, body, 15000, 'trustedHtml');
    };

    $rootScope.showNote = function (title, body) {
        toaster.pop('note', title, body, 15000, 'trustedHtml');
    };
}]);

app.run(['$rootScope', '$location', function ($rootScope, $location) {
    $rootScope.CheckThisIsCurrentUrl = function (url) {
        if (url === $location.url())
            return true;
        else
            return false;
    }
}]);

app.run(['$rootScope', '$http', 'localStorageService', '$hashService',
      function ($rootScope, $http, localStorageService, $hashService) {
          $rootScope.$on('$locationChangeSuccess', function (event) {

              $http.get('Main/GetCurrentUser').success(function (data) {
                  $rootScope.userId = data.Id;
              });

              $rootScope.showWait();
              $http.get('account/IsAuthorized').success(function (data) {
                  $rootScope.authState = data;
                  if ($rootScope.authState) {
                      var value = localStorageService.get('Privilages');

                      $http.get('account/GetCurrentUserPrivilages').success(function (data) {
                          localStorageService.set("Privilages", data);

                          $rootScope.isViewPatientAllow = $hashService.isPrivilageAllow(1);
                          $rootScope.isCreatePatientAllow = $hashService.isPrivilageAllow(2);
                          $rootScope.isEditPatientAllow = $hashService.isPrivilageAllow(3);
                          $rootScope.isDeletePatientAllow = $hashService.isPrivilageAllow(4);
                          $rootScope.isPrintReportAllow = $hashService.isPrivilageAllow(5);
                          $rootScope.isViewUsersAllow = $hashService.isPrivilageAllow(6);
                          $rootScope.isCreateUserAllow = $hashService.isPrivilageAllow(7);
                          $rootScope.isEditUserAllow = $hashService.isPrivilageAllow(8);
                          $rootScope.isDeleteUserAllow = $hashService.isPrivilageAllow(9);
                          $rootScope.isViewRolesAllow = $hashService.isPrivilageAllow(10);
                          $rootScope.isCreateRoleAllow = $hashService.isPrivilageAllow(11);
                          $rootScope.isEditRoleAllow = $hashService.isPrivilageAllow(12);
                          $rootScope.isDeleteRoleAllow = $hashService.isPrivilageAllow(13);
                          $rootScope.isViewCitiesAllow = $hashService.isPrivilageAllow(14);
                          $rootScope.isCreateCityAllow = $hashService.isPrivilageAllow(15);
                          $rootScope.isEditCityAllow = $hashService.isPrivilageAllow(16);
                          $rootScope.isDeleteCityAllow = $hashService.isPrivilageAllow(17);
                          $rootScope.isDownloadAllow = $hashService.isPrivilageAllow(18);
                          $rootScope.isReportsAllow = $hashService.isPrivilageAllow(19);

                          $rootScope.hideWait();
                      });
                  }
              });
          });
      }]);

app.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];
        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

app.factory('$hashService', ['$odata', '$odataresource', 'localStorageService', '$http', '$rootScope',
    function ($odata, $odataresource, localStorageService, $http, $rootScope) {
        return {
            getAllData: function (tblName, showDeleted) {
                return $odataresource('odata/' + tblName, {},
                       {
                           odata: {
                               method: 'GET',
                               isArray: true,
                               transformResponse: function (data) {
                                   return angular.fromJson(data).value;
                               }
                           }
                       })
                       .odata().filter(
                        'isDeleted',
                        new $odata.Value(showDeleted, "Boolean")
                        ).query();

            },
            getSingleEntity: function (tblName, id) {
                var Entity = $odataresource('odata/' + tblName, { userId: '@id' });
                return Entity.odata().get(id);
            },
            getSingleColumn: function (tblName, columnName) {
                var Entity = $odataresource('odata/' + tblName, {},
                     {
                         odata: {
                             method: 'GET',
                             isArray: true,
                             transformResponse: function (data) {
                                 return angular.fromJson(data).value;
                             }
                         }
                     });
                return Entity.odata().select(columnName).query();
            },
            postItem: function (tblName, obj) {
                Entity = $odataresource('odata/' + tblName, {}, {}, { odatakey: 'id' });
                var myEntity = new Entity(obj);
                myEntity.$save();
            },
            putItem: function (tblName, obj, id) {
                Entity = $odataresource('odata/' + tblName + '(' + id + ')', {}, { odatakey: 'id' });
                var myEntity = new Entity(obj);
                myEntity.$update();
            },
            deleteItem: function (tblName, id) {
                Entity = $odataresource('odata/' + tblName + '(' + id + ')', {}, { odatakey: 'id' });
                var myEntity = new Entity();
                myEntity.$delete();
            },
            neglect: function (tblName, obj, id) {
                Entity = $odataresource('odata/' + tblName + '(' + id + ')', {}, {}, { odatakey: 'id', isodatav4: true });
                var myEntity = new Entity(obj);
                myEntity.$update();
            },
            restore: function (tblName, obj, id) {
                Entity = $odataresource('odata/' + tblName + '(' + id + ')', {}, {}, { odatakey: 'id' });
                var myEntity = new Entity(obj);
                myEntity.$update();
            },
            isPrivilageAllow: function (id) {
                var privs = localStorageService.get('Privilages');
                for (var i = 0; i < privs.length; i++) {
                    if (privs[i].PrivilageId === id) {
                        return privs[i].isAllowed;
                    }
                }
            },
            postUserAction: function (userId, ActionDetails) {
                var userAction =
                    {
                        Id: userId,
                        ActionDetails: ActionDetails
                    }
                $http.post('/api/UserActions', userAction).success(function () {

                }).error(function () {
                    $rootScope.showError('User Action Error', 'cannt log action in db');
                });
            }
        };
    }]);

app.controller('indexCtrl', ['$rootScope', '$scope',
    function ($rootScope, $scope) {
    }]);

app.controller('mapCtrl', ['$rootScope', '$scope', '$http',
    function ($rootScope, $scope, $http) {
        var states = [
       "Ar Riyad",  //"SA-01", 
       "Makkah", //"SA-02", 
       "Al Madinah", //"SA-03", 
       "Ash Sharqiyah", //"SA-04", 
       "Al Qasim", //"SA-05", 
       "Haeil", //"SA-06", 
       "Tabouk", //"SA-07", 
       "Al Hudud ash Shamalisyah",  //"SA-08", 
       "Jizan", //"SA-09", 
       "Najran",  //"SA-10", 
       "Al Bahah", //"SA-11", 
       "Al Jawf",  // "SA-12",
       "Asir" // "SA-14" 
        ];

        $scope.createDummyData = function () {

            $http.get('Main/GetMapData').success(function (data) {
                $scope.patientMaleInCity = data.PatientMaleInCity;
                $scope.patientFemaleInCity = data.PatientFemaleInCity;

                var ss = $scope.patientMaleInCity.replace(/"/g, "").replace(/[{}]/g, "").split(',');
                $rootScope.allItems = [];
                for (var i = 0; i < ss.length; i++) {
                    $scope.item = ss[i].toString().split(':')[1];
                    $rootScope.allItems.push($scope.item);
                }

                var female = $scope.patientFemaleInCity.replace(/"/g, "").replace(/[{}]/g, "").split(',');
                $rootScope.allFemaleItems = [];
                for (var j = 0; j < female.length; j++) {
                    $scope.item = female[j].toString().split(':')[1];
                    $rootScope.allFemaleItems.push($scope.item);
                }

                $scope.index = 0;
                var dataTemp = {};
                angular.forEach(states, function (state, key) {
                    dataTemp[state] = { value: $rootScope.allItems[$scope.index], female: $rootScope.allFemaleItems[$scope.index] }
                    $scope.index++;
                });
                $scope.dummyData = dataTemp;
            });

        };
        $scope.createDummyData();

        $scope.changeHoverRegion = function (region) {
            $scope.hoverRegion = region;
        };
    }]);

app.controller('profileCtrl', ['$scope', '$http', '$rootScope', '$window',
    function ($scope, $http, $rootScope, $window) {

        $scope.profile = {
            OldPassword: "",
            NewPassword: ""
        };
        $scope.savingEditProfile = false;

        $scope.EditProfile = function () {
            $scope.savingEditProfile = true;

            $http.post('Manage/ChangePassword', $scope.profile).success(function (data) {
                $scope.savingEditProfile = false;
                $rootScope.showSuccess('Successfully Chang The Password', 'a New passwoed Has been added this moment sucessfully');
                $window.location.href = '#/';
            }).error(function (data) {
                $scope.savingEditProfile = false;
                $scope.errorInPassword = "Your old password is wrong please check it";
            });
        }


    }]);

app.controller('addFormCtrl', ['$rootScope', '$scope', '$hashService', '$http', 'toaster', '$window', '$upload', '$q',
     function ($rootScope, $scope, $hashService, $http, toaster, $window, $upload, $q) {
         $window.scrollTo(0, 0);
         $scope.patient = { FileUrl: "" };
         $scope.patientinfoshow = true;
         $scope.riskinfoshow = true;
         $scope.diagonisticsinfoshow = true;
         $scope.osteoporosisinfoshow = true;


         $scope.allCities = function () {
             $http.get('api/Cities').success(function (data) {
                 $rootScope.cities = [];
                 for (var i = 0; i < data.length; i++) {
                     if (data[i].isDeleted === false) {
                         $rootScope.cities.push(data[i]);
                     }
                 }
             }).error(function () {
                 $rootScope.showError('Unable to load Data ', 'The System was Unable to bound Data From the Server .');
             });
         }
         $scope.allCities();

         $scope.adminCities = function () {
             $http.get('Main/GetAdminCity').success(function (data) {
                 $rootScope.adminCity = data;
             }).error(function () {
                 $rootScope.showError('Unable to load Data ', 'The System was Unable to bound Data From the Server .');
             });
         }
         $scope.adminCities();

         $scope.gender = ''; //false
         $scope.occupation = ''; //Unemployed
         $scope.Education = '';  //Un_educated
         $scope.HasFracture = ''; //false
         $scope.OsteoporosisCase = ''; //Normal

         $scope.caseStatusArr = ['OsteoporosisWithFracture', 'Osteoporosis', 'Osteopenia', 'Normal'];
         $scope.occupationArr = ['Unemployed', 'Student', 'Professional', 'Manual_Labor', 'Unknown', 'Other_Specify'];
         $scope.EducationArr = ['Un_educated', 'Read_Write', 'Primary', 'Intermediate', 'Secondary', 'University'];

         $scope.calcDmi = function () {
             var H = $scope.patient.Height / 100;
             var DMI = $scope.patient.Weight / (H * H);
             $scope.resultDMI = DMI.toFixed(2);
         }


         $scope.dateOptions = {
             formatYear: 'yy',
             maxDate: new Date(),
             minDate: new Date(1880),
             startingDay: 1
         };

         $scope.popup1 = {
             opened: false
         };

         $scope.popup2 = {
             opened: false
         };

         $scope.open1 = function () {
             $scope.popup1.opened = true;
         };

         $scope.open2 = function () {
             $scope.popup2.opened = true;
         };
         $scope.format = 'dd/MM/yyyy';
         $scope.altInputFormats = ['M!/d!/yyyy'];

         $scope.todayTest = function () {
             $scope.TestDate = new Date();
         };
         $scope.todayTest();

         $scope.todayBirth = function () {
             $scope.BirthDate = new Date();
         };
         $scope.todayBirth();

         $scope.clearTest = function () {
             $scope.TestDate = null;
         };

         $scope.clearBirth = function () {
             $scope.BirthDate = null;
         };

         function getDayClass(data) {
             var date = data.date,
               mode = data.mode;
             if (mode === 'day') {
                 var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                 for (var i = 0; i < $scope.events.length; i++) {
                     var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                     if (dayToCheck === currentDay) {
                         return $scope.events[i].status;
                     }
                 }
             }
             return '';
         }

         $scope.risk = '';
         $scope.riskModel = {
             LowVitamin: false,
             HormoneTherapy: false,
             Smoking: false,
             RheumatoidArthritis: false,
             Glucocorticoid: false,
             SecondryCauses: false,
             PersonalHistoryOfFragilityFracture: false,
             FamilyHistoryOfOsteoporoticFracture: false,
             LowCalciumIntakelesThanTwoServings: false

         };
         $scope.formValid = false;
         $scope.checkResults = [];
         $scope.validateRisk = function () {
             return ($scope.riskModel.LowVitamin || $scope.riskModel.HormoneTherapy || $scope.riskModel.Smoking || $scope.riskModel.RheumatoidArthritis || $scope.riskModel.Glucocorticoid || $scope.riskModel.SecondryCauses || $scope.riskModel.PersonalHistoryOfFragilityFracture || $scope.riskModel.FamilyHistoryOfOsteoporoticFracture || $scope.riskModel.LowCalciumIntakelesThanTwoServings);
         }
         $scope.validatOccupation = function () {
             return $scope.occupation !== '' && $scope.occupation !== undefined;
         }
         $scope.validatGender = function () {
             return $scope.gender !== '' && $scope.gender !== undefined;
         }
         $scope.validatEducation = function () {
             return $scope.Education !== '' && $scope.Education !== undefined;
         }
         $scope.validatHasFracture = function () {
             return $scope.HasFracture !== '' && $scope.HasFracture !== undefined;
         }
         $scope.validatOsteoporosisCase = function () {
             return $scope.OsteoporosisCase !== '' && $scope.OsteoporosisCase !== undefined;
         }
         $scope.uploadedFiles = [];
         $scope.upload = [];
         var files = [];
         $scope.onFileSelect = function ($files) {
             files = $files;
             console.log($scope.uploadedFiles);
         }
         $scope.clear = function () {
             $scope.File = null;
             $scope.uploadedFiles = [];
             angular.element("#file").val(null);
         }
         // function to add patient data in db
         $scope.saving = false;
         $scope.add = function () {
             if ($scope.addForm.$valid && $scope.validateRisk() && $scope.validatOccupation()
                 && $scope.validatGender() && $scope.validatEducation() && $scope.validatOsteoporosisCase()) {
                 var filesLength = files.length;
                 for (var i = 0; i < filesLength; i++) {
                     var $file = files[i];
                     (function (index) {
                         $scope.upload[index] = $upload.upload({
                             url: "./api/files/upload", // webapi url
                             method: "POST",
                             file: $file
                         });
                     })(i);
                 }
                 $scope.File = '';
                 $q.all($scope.upload).then(function (results) {
                     for (var i = 0; i < results.length; i++) {
                         $scope.File += results[i].data.returnData + ((i == results.length - 1) ? "" : ",");
                     }
                     $scope.saving = true;
                     $rootScope.showWait();
                     $scope.patient.Occupation = $scope.occupationArr.indexOf($scope.occupation);
                     $scope.patient.PatientGender = Boolean($scope.gender);
                     $scope.patient.Education = $scope.EducationArr.indexOf($scope.Education);
                     angular.forEach($scope.riskModel, function (value, key) {
                         if (value) {
                             $scope.checkResults.push(key);
                         }
                     });
                     var checkStr = $scope.checkResults.toString();
                     $scope.patient.BodyMassIndex = checkStr;
                     $scope.patient.OsteoporosisCase = $scope.caseStatusArr.indexOf($scope.OsteoporosisCase);
                     $scope.patient.HasFracture = Boolean($scope.hasFracture);

                     $scope.patient.RiskFactor = ($scope.resultDMI);
                     $scope.patient.isDeleted = false;

                     var pat = {
                         PatientFirstName: $scope.patient.PatientFirstName,
                         PatientFatherName: $scope.patient.PatientFatherName,
                         PatientLastName: $scope.patient.PatientLastName,
                         PatientGender: Boolean($scope.gender),
                         NationalId: $scope.patient.NationalId,
                         TestDate: window.moment($scope.patient.TestDate),
                         BirthDate: window.moment($scope.patient.BirthDate),
                         CreationDate: window.moment(new Date()),
                         ContactNumber: $scope.patient.ContactNumber,
                         MobileNumber: $scope.patient.MobileNumber,
                         Occupation: $scope.occupationArr.indexOf($scope.occupation),
                         Education: $scope.EducationArr.indexOf($scope.Education),
                         RiskFactor: ($scope.resultDMI),
                         Height: $scope.patient.Height,
                         Weight: $scope.patient.Weight,
                         BodyMassIndex: checkStr,
                         HasFracture: Boolean($scope.hasFracture),
                         DXALumber: $scope.patient.DXALumber,
                         DXAFemoral: $scope.patient.DXAFemoral,
                         DXATotalHip: $scope.patient.DXATotalHip,
                         BMDLumber: $scope.patient.BMDLumber,
                         BMDFemoral: $scope.patient.BMDFemoral,
                         BMDTotalHip: $scope.patient.BMDTotalHip,
                         OsteoporosisCase: $scope.caseStatusArr.indexOf($scope.OsteoporosisCase),
                         isDeleted: false,
                         LiveCityId: $scope.patient.LiveCityId,
                         ServiceCityId: 0,
                         FileUrl: $scope.File
                     }
                     $http.post('Main/PostPatient', pat).success(function () {
                         $rootScope.showSuccess('Successfully Register New Patient', 'a New patient Has been registerd this moment sucessfully');
                         $hashService.postUserAction($rootScope.userId, 'Create a new patient');

                         // Invoke getAllPatient method
                         $http.get('Main/GetPatientInCity/').success(function (data) {
                             $rootScope.list = [];
                             for (var i = 0; i < data.length; i++) {
                                 if (data[i].isDeleted === $scope.showhistory) {
                                     $rootScope.list.push(data[i]);
                                 }
                             }
                             $window.location.href = '#/allForms';
                             $scope.saving = false;
                             $window.scrollTo(0, 0);
                             $rootScope.hideWait();
                         }).error(function () {
                             $rootScope.showError('Unable to load Patients Data', 'The System Was Unable to retrieve the patient Data From the Server .');
                         });
                     }).error(function () {
                         $scope.saving = false;
                         $rootScope.hideWait();
                     });
                 });
             }
             else {
                 $rootScope.showError('Complte Patient Information', 'There is Some Required Information not Provided pleas Review your Form .');
             }
         }

         $scope.reset = function () {
             $scope.patient.PatientFirstName = '';
             $scope.patient.PatientFatherName = '';
             $scope.patient.PatientLastName = '';
             $scope.gender = '';
             $scope.patient.NationalId = '';
             $scope.patient.TestDate = '';
             $scope.patient.BirthDate = '';
             $scope.patient.ContactNumber = '';
             $scope.patient.MobileNumber = '';
             $scope.occupation = '';
             $scope.Education = '';
             $scope.resultDMI = '';
             $scope.riskModel = '';
             $scope.fracture = '';
             $scope.patient.DXALumber = '';
             $scope.patient.DXAFemoral = '';
             $scope.patient.DXATotalHip = '';
             $scope.patient.BMDLumber = '';
             $scope.patient.BMDFemoral = '';
             $scope.patient.BMDTotalHip = '';
             $scope.OsteoporosisCase = '';
             $scope.patient.LiveCityId = '';
             $scope.patient.ServiceCityId = '';
             $scope.addForm.$setPristine();
             $scope.addForm.$setUntouched();
         };

         $scope.emptyBirthDate = function () {
             $scope.patient.BirthDate = '';
         }

         $scope.errInAge = false;
         $scope.calculateAge = function (birthday) {
             var ageDifMs = Date.now() - new Date(birthday);
             var ageDate = new Date(ageDifMs); // miliseconds from epoch
             var age = Math.abs(ageDate.getUTCFullYear() - 1970);
             if (age >= 18) {
                 $scope.errorInAge = age + "years old";
                 $scope.errInAge = true;
             } else {
                 $scope.errInAge = true;
                 $scope.errorInAge = "You must choose brith data with age 18 years old at minim";
                 $scope.patient.BirthDate = '';
             }
         }
     }]);

app.controller('homeCtrl', ['$rootScope', '$scope', '$hashService', '$http',
    function ($rootScope, $scope, $hashService, $http) {
        $scope.GetSatatistics = function () {
            $http.get('Main/GetStastics').success(function (data) {
                $scope.numForm = data.PatientsNum;
                $scope.numUser = data.UsersNum;
                $scope.numRole = data.RolesNum;
                $scope.numCity = data.CitiesNum;
                $scope.donutchartsData = data.DonutchartsData.items;
                $scope.barGenderData = data.BarGenderData.items;
                $scope.patientInCity = data.PatientInCity;
                $scope.riskFactorCases = data.RiskFactorCases.items;



                $scope.genderData =
                [
                    { y: 'OsteoporosisWithFracture', a: $scope.barGenderData[0].valueMale, b: $scope.barGenderData[0].valueFemale },
                    { y: 'Osteoporosis', a: $scope.barGenderData[1].valueMale, b: $scope.barGenderData[1].valueFemale },
                    { y: 'Osteopenia', a: $scope.barGenderData[2].valueMale, b: $scope.barGenderData[2].valueFemale },
                    { y: 'Normal', a: $scope.barGenderData[3].valueMale, b: $scope.barGenderData[3].valueFemale }
                ];
                $scope.xkey = 'y';
                $scope.ykeys = ['a', 'b'];

                // --------------------------------- ----------------------------
                var ss = $scope.patientInCity.replace(/"/g, "").replace(/[{}]/g, "").split(',');
                $scope.cityData = [];
                for (var i = 0; i < ss.length; i++) {
                    var item = ss[i].toString().split(':');
                    var item1 = { x: item[0], c: item[1] };
                    $scope.cityData.push(item1);
                }

                $scope.xaxis = 'x';
                $scope.yaxis = '["c"]';
                // --------------------------------- -------------------------------

                $scope.riskData =
                [
                    { z: 'Low Vitamin', d: $scope.riskFactorCases[0].value1, e: $scope.riskFactorCases[0].value2, f: $scope.riskFactorCases[0].value3, g: $scope.riskFactorCases[0].value4 },
                    { z: 'Hormone Therapy', d: $scope.riskFactorCases[1].value1, e: $scope.riskFactorCases[1].value2, f: $scope.riskFactorCases[1].value3, g: $scope.riskFactorCases[1].value4 },
                    { z: 'Smoking', d: $scope.riskFactorCases[2].value1, e: $scope.riskFactorCases[2].value2, f: $scope.riskFactorCases[2].value3, g: $scope.riskFactorCases[2].value4 },
                    { z: 'RheumatoidArthrit', d: $scope.riskFactorCases[3].value1, e: $scope.riskFactorCases[3].value2, f: $scope.riskFactorCases[3].value3, g: $scope.riskFactorCases[3].value4 },
                    { z: 'Glucocorticoid ', d: $scope.riskFactorCases[4].value1, e: $scope.riskFactorCases[4].value2, f: $scope.riskFactorCases[4].value3, g: $scope.riskFactorCases.value4 },
                    { z: 'Secondry Causes', d: $scope.riskFactorCases[5].value1, e: $scope.riskFactorCases[5].value2, f: $scope.riskFactorCases[5].value3, g: $scope.riskFactorCases[5].value4 },
                    { z: 'Personal History', d: $scope.riskFactorCases[6].value1, e: $scope.riskFactorCases[6].value2, f: $scope.riskFactorCases[6].value3, g: $scope.riskFactorCases[6].value4 },
                    { z: 'Family History', d: $scope.riskFactorCases[7].value1, e: $scope.riskFactorCases[7].value2, f: $scope.riskFactorCases[7].value3, g: $scope.riskFactorCases[7].value4 },
                    { z: 'low Calcium', d: $scope.riskFactorCases[8].value1, e: $scope.riskFactorCases[8].value2, f: $scope.riskFactorCases[8].value3, g: $scope.riskFactorCases[8].value4 }
                ];
                $scope.xaxisf = 'z';
                $scope.yaxisf = ['d', 'e', 'f', 'g'];

            });

        }
        $scope.GetSatatistics();


    }]);

app.controller('allFormsCtrl', ['$rootScope', '$scope', '$hashService', '$uibModal', '$odataresource', '$odata', '$window', '$http',
    function ($rootScope, $scope, $hashService, $uibModal, $odataresource, $odata, $window, $http) {
        $window.scrollTo(0, 0);
        $scope.sortKey = 'id';
        $scope.reverse = false;
        $scope.num = 10;
        $scope.filter = false;
        $scope.showData = true;
        $scope.showhistory = false;

        $scope.setView = function (index) {
            $scope.showData = false;
            $scope.showhistory = false;
            switch (index) {
                case 0:
                    $scope.showData = true;
                    break;
                case 1:
                    $scope.showhistory = true;
                    break;
                default:
            }
            $scope.allPatient();
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        };
        $scope.triggerFilter = function () {
            $scope.filter = $scope.filter ? false : true;
        }

        $scope.allPatient = function () {
            $rootScope.showWait();
            $http.get('Main/GetPatientInCity/').success(function (data) {
                $rootScope.list = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDeleted === $scope.showhistory) {
                        $rootScope.list.push(data[i]);
                    }
                }
                $rootScope.hideWait();
            }).error(function () {
                $rootScope.showError('Unable to load Patients Data', 'The System Was Unable to retrieve the patient Data From the Server .');
                $rootScope.hideWait();
            });
        }
        $scope.allPatient();

        // Neglect patient 
        $scope.setDelete = function (item) {
            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete This Patient ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.deletedItem = item;
                    var id = $scope.deletedItem.id;
                    $http.get('api/Patients/' + id).success(function (data) {
                        $scope.actualItem = data;
                        $scope.delete = function () {
                            $scope.actualItem.isDeleted = true;
                            $http.put('api/Patients/' + id, $scope.actualItem).success(function () {
                                $rootScope.showSuccess('Success Neglect Patient', 'The operation of neglect patient done successfully');
                                $hashService.postUserAction($rootScope.userId, 'Delete an existing patient');
                                var index = $rootScope.list.indexOf($scope.deletedItem);
                                $rootScope.list.splice(index, 1);
                                $uibModalInstance.close();
                            }).error(function () {
                                $rootScope.showError('Error Neglect Patient', 'The operation of neglect patient is falied');
                            });
                        };
                    });
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

        // Download patient file
        $scope.DownloadFile = function (item) {
            if (item.fileUrl !== '' && item.fileUrl !== null) {
                var url = '/Main/Download/' + item.id;
                var win = window.open(url, '_blank');
                win.focus();
            }
        };

        // Print Patient 
        $scope.PrintReport = function (item) {
            var url = '/Main/PatientReport/' + item.id;
            var win = window.open(url, '_blank');
            win.focus();
        };

        // Delete patient permantly
        $scope.Delete = function (item) {
            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete This Patient ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.deletedItem = item;

                    $scope.delete = function () {

                        $http.delete('api/Patients/' + $scope.deletedItem.id).success(function () {
                            $rootScope.showSuccess('Success Delete Patient', 'The operation of delete patient done successfully');
                        }).error(function () {
                            $rootScope.showError('Error Delete Patient', 'The operation of delete patient failed');
                        });

                        $hashService.postUserAction($rootScope.userId, 'Delete an existing patient permantly');
                        var index = $rootScope.list.indexOf($scope.deletedItem);
                        $rootScope.list.splice(index, 1);
                        $uibModalInstance.close();

                    };

                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

        // Restore patient
        $scope.restorePatient = function (item) {
            var restoreTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Restore</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Restoe This Patient ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="rest()">Restore</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: restoreTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

                    $scope.restoredPatient = item;
                    var id = $scope.restoredPatient.id;

                    $http.get('api/Patients/' + id).success(function (data) {
                        $scope.actualItem = data;
                        $scope.rest = function () {
                            $scope.actualItem.isDeleted = false;
                            $http.put('api/Patients/' + id, $scope.actualItem).success(function () {
                                $rootScope.showSuccess('Success Restore Patient', 'The operation of restore patient done successfully');
                            }).error(function () {
                                $rootScope.showError('Error Restore Patient', 'The operation of restore patient failed');
                            });
                            $hashService.postUserAction($rootScope.userId, 'Delete an existing patient');
                            var index = $rootScope.list.indexOf($scope.deletedItem);
                            $rootScope.list.splice(index, 1);
                            $uibModalInstance.close();
                        };
                    });
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

    }]);

app.controller('previewCtrl', ['$rootScope', '$scope', '$location', '$hashService', '$odataresource', '$window', '$http',
    function ($rootScope, $scope, $location, $hashService, $odataresource, $window, $http) {

        $window.scrollTo(0, 0);
        $scope.id = $location.search().id;
        $rootScope.showWait();
        $rootScope.hideWait();

        $scope.EducationArr = ['Un_educated', 'Read_Write', 'Primary', 'Intermediate', 'Secondary', 'University'];
        $scope.caseStatusArr = ['OsteoporosisWithFracture', 'Osteoporosis', 'Osteopenia', 'Normal'];
        $scope.occupationArr = ['Unemployed', 'Student', 'Professional', 'Manual_Labor', 'Unknown', 'Other_Specify'];

        $scope.DownloadFile = function () {
            var url = '/Main/Download/' + $scope.id;
            var win = window.open(url, '_blank');
            win.focus();
        };
        $scope.PrintReport = function () {
            var url = '/Main/PatientReport/' + $scope.id;
            var win = window.open(url, '_blank');
            win.focus();
        };
        $rootScope.showWait();

        $scope.loadAllData = function () {
            $http.get('/api/Patients/' + $scope.id).success(function (data) {
                $http.get('/api/Cities/' + data.liveCityId).success(function (livecity) {
                    $scope.liveCity = livecity.cityName;
                    $http.get('/api/Cities/' + data.serviceCityId).success(function (servicecity) {
                        $scope.serviceCity = servicecity.cityName;

                        $scope.patient = {
                            PatientId: data.id,
                            FullName: data.patientFirstName + data.patientFatherName + data.patientLastName,
                            NationalId: data.nationalId,
                            PatientGender: data.patientGender ? 'male' : 'female',
                            OsteoporosisCase: $scope.caseStatusArr[data.osteoporosisCase],
                            Education: $scope.EducationArr[data.education],
                            Occupation: $scope.occupationArr[data.occupation],
                            ServiceCity: $scope.serviceCity,
                            LiveCity: $scope.liveCity,
                            Height: data.height,
                            Weight: data.weight,
                            RiskFactor: data.riskFactor,
                            BodyMassIndex: data.bodyMassIndex.replace(',', ' - '),
                            DXALumber: data.dxaLumber,
                            DXAFemoral: data.dxaFemoral,
                            DXATotalHip: data.dxaTotalHip,
                            BMDLumber: data.bmdLumber,
                            BMDFemoral: data.bmdFemoral,
                            BMDTotalHip: data.bmdTotalHip,
                            TestDate: data.testDate,
                            BirthDate: data.birthDate,
                            HasFracture: data.hasFracture ? 'Yes' : 'No'

                        };
                        $rootScope.hideWait();
                    });
                });
            }).error(function () {
                $rootScope.showError("Oooooops, something wrong happened during load Patient Data From the Server.!");
                $rootScope.hideWait();
            });
        };
        $scope.loadAllData();

        $http.get('Main/GetCurrentUser').success(function (data) {
            $rootScope.userId = data.Id;

            $http.get('/api/UserActions/' + $rootScope.userId).success(function (data) {
                $scope.userAction = data.userName;
                $scope.actionDate = data.actionDate;

            }).error(function () {

            });
        });
    }]);

app.controller('editPatientCtrl', ['$rootScope', '$scope', '$location', '$hashService', '$odataresource', '$window', 'toaster', '$filter', '$http', '$upload',
    function ($rootScope, $scope, $location, $hashService, $odataresource, $window, toaster, $filter, $http, $upload) {
        // =========== Get the Id from Query Sring var ==============
        $scope.id = $location.search().id;

        $scope.EducationArr = ['Un_educated', 'Read_Write', 'Primary', 'Intermediate', 'Secondary', 'University'];
        $scope.caseStatusArr = ['OsteoporosisWithFracture', 'Osteoporosis', 'Osteopenia', 'Normal'];
        $scope.occupationArr = ['Unemployed', 'Student', 'Professional', 'Manual_Labor', 'Unknown', 'Other_Specify'];
        $scope.gender = '';
        $scope.fracture = '';
        $scope.risk = '';
        $scope.riskModel = {
            LowVitamin: false,
            HormoneTherapy: false,
            Smoking: false,
            RheumatoidArthritis: false,
            Glucocorticoid: false,
            PersonalHistoryOfFragilityFracture: false,
            FamilyHistoryOfOsteoporoticFracture: false,
            LowCalciumIntakelesThanTwoServings: false,
            SecondryCauses: false
        };
        $scope.checkResults = [];

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(),
            minDate: new Date(1800, 12, 30),
            startingDay: 1
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.format = 'dd/MM/yyyy';
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.todayTest = function () {
            $scope.TestDate = new Date();
        };
        $scope.todayTest();

        $scope.todayBirth = function () {
            $scope.BirthDate = new Date();
        };
        $scope.todayBirth();

        $scope.clearTest = function () {
            $scope.TestDate = null;
        };

        $scope.clearBirth = function () {
            $scope.BirthDate = null;
        };

        function getDayClass(data) {
            var date = data.date,
              mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }
            return '';
        }
        $scope.validateRisk = function () {
            return ($scope.riskModel.LowVitamin || $scope.riskModel.HormoneTherapy || $scope.riskModel.Smoking || $scope.riskModel.RheumatoidArthritis || $scope.riskModel.Glucocorticoid || $scope.riskModel.SecondryCauses || $scope.riskModel.PersonalHistoryOfFragilityFracture || $scope.riskModel.FamilyHistoryOfOsteoporoticFracture || $scope.riskModel.LowCalciumIntakelesThanTwoServings);
        }
        $scope.validatOccupation = function () {
            return $scope.occupation !== '' && $scope.occupation !== undefined;
        }

        $scope.getAdminCity = function () {
            $http.get('Main/GetAdminCity').success(function (data) {
                $rootScope.adminCity = data;
            });
        }
        $scope.getAdminCity();


        $scope.loadPatientData = function () {
            $rootScope.showWait();

            $http.get('api/Cities').success(function (data) {
                $rootScope.cities = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDeleted === true) {
                        $rootScope.cities.push(data[i]);
                    }
                }

                $http.get('api/Patients/' + $scope.id).success(function (data) {

                    $scope.patient = data;
                    $scope.PatientGender = data.patientGender ? 'true' : 'false';
                    $scope.Occupation = $scope.occupationArr[data.occupation];
                    $scope.Education = $scope.EducationArr[data.education];
                    $scope.OsteoporosisCase = $scope.caseStatusArr[data.osteoporosisCase];
                    $scope.HasFracture = data.hasFracture ? 'true' : 'false';

                    $scope.resultDMI = data.riskFactor;
                    $scope.ServiceCityId = data.serviceCityId;
                    $scope.LiveCityId = data.liveCityId;
                    $scope.BirthDate = new Date(data.birthDate);
                    $scope.TestDate = new Date(data.testDate);

                    $scope.filesArr = [];
                    if (data.fileUrl != null) {
                        $scope.filesArr = data.fileUrl.split(',');
                    }

                    var BodyMassIndexs = data.bodyMassIndex;
                    var values = BodyMassIndexs.split(',');
                    for (var i = 0; i < values.length; i++) {
                        switch (values[i]) {
                            case 'LowVitamin': $scope.riskModel.LowVitamin = true; break;
                            case 'HormoneTherapy': $scope.riskModel.HormoneTherapy = true; break;
                            case 'Smoking': $scope.riskModel.Smoking = true; break;
                            case 'RheumatoidArthritis': $scope.riskModel.RheumatoidArthritis = true; break;
                            case 'Glucocorticoid': $scope.riskModel.Glucocorticoid = true; break;
                            case 'PersonalHistoryOfFragilityFracture': $scope.riskModel.PersonalHistoryOfFragilityFracture = true; break;
                            case 'FamilyHistoryOfOsteoporoticFracture': $scope.riskModel.FamilyHistoryOfOsteoporoticFracture = true; break;
                            case 'LowCalciumIntakelesThanTwoServings': $scope.riskModel.LowCalciumIntakelesThanTwoServings = true; break;
                            case 'SecondryCauses': $scope.riskModel.SecondryCauses = true; break;
                            default:
                        }
                    }
                    $rootScope.hideWait();
                }).error(function () {
                    $rootScope.hideWait();
                    $rootScope.showError('oooPs I cant Perform this Action .', 'I cant perform this action for now you can try again soon .');
                }); // end of patient data



            }); // end of get all cities
        }
        $scope.loadPatientData();


        $scope.upload = [];
        $scope.onFileSelect = function ($files) {
            //$files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < $files.length; i++) {
                var $file = $files[i];
                (function (index) {
                    $scope.upload[index] = $upload.upload({
                        url: "./api/files/upload", // webapi url
                        method: "POST",
                        file: $file
                    }).progress(function (evt) {
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    }).success(function (data, status, headers, config) {
                        $scope.FileUrl = data.returnData;
                        //window.filesUploded.push(data.returnData);
                        console.log($scope.FileUrl);
                        //window.initfileuploader();
                        //console.log($scope.File);
                    }).error(function (data, status, headers, config) {
                        $rootScope.showError('Cant Upload File', 'The System Cant Upload The File to the Server Please Try to upload File again...');
                    });
                })(i);
            }
        }
        $scope.clear = function () {
            $scope.File = null;
            angular.element("#file").val(null);
        }

        $scope.saving = false;
        $scope.add = function () {
            if ($scope.addForm.$valid) {
                $scope.saving = true;
                $rootScope.showWait();
                $scope.patient.ServiceCityId = $scope.ServiceCityId;
                $scope.patient.LiveCityId = $scope.LiveCityId;
                $scope.patient.Occupation = $scope.occupationArr.indexOf($scope.Occupation);
                $scope.patient.OsteoporosisCase = $scope.caseStatusArr.indexOf($scope.OsteoporosisCase);
                $scope.patient.Education = $scope.EducationArr.indexOf($scope.Education);
                $scope.patient.HasFracture = $scope.HasFracture;
                $scope.patient.PatientGender = $scope.PatientGender;
                $scope.patient.RiskFactor = ($scope.resultDMI);
                angular.forEach($scope.riskModel, function (value, key) {
                    if (value) {
                        $scope.checkResults.push(key);
                    }
                });
                var checkStr = $scope.checkResults.toString();
                $scope.patient.BodyMassIndex = checkStr;
                $scope.patient.BirthDate = $scope.BirthDate;
                $scope.patient.TestDate = $scope.TestDate;
                $scope.patient.FileUrl = $scope.FileUrl;

                $http.put('/api/Patients/' + $scope.id, $scope.patient).success(function (data) {
                    $hashService.postUserAction($rootScope.userId, 'Edit an existing patient');

                    $rootScope.showSuccess('Saving Patient Info success', 'Edit operation done successfully');
                    $scope.saving = false;
                    $rootScope.hideWait();
                }).error(function () {
                    $scope.saving = false;
                    $rootScope.showError('unknown error occured ', 'ooops there is some problem in saving your patient')
                    $rootScope.hideWait();
                });
            }
            else {
                $rootScope.showError('Complte Patient Information', 'There is Some Required Information not Provided or Invalid, Please Review your Form .');
            }
        }

        $scope.cancel = function () {
            $window.location.href = '#/allForms';
            $window.scrollTo(0, 0);
        }

        $scope.errInAge = false;
        $scope.calculateAge = function (birthday) {
            var ageDifMs = Date.now() - new Date(birthday);
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            var age = Math.abs(ageDate.getUTCFullYear() - 1970);
            if (age >= 18) {
                $scope.errorInAge = age + "years old";
                $scope.errInAge = true;
            } else {
                $scope.errInAge = true;
                $scope.errorInAge = "You must choose brith data with age 18 years old at minim";
                $scope.BirthDate = '';
            }
        }
    }]);

app.controller('allCitiesCtrl', ['$rootScope', '$scope', '$hashService', '$uibModal', '$window', '$odataresource', '$http',
    function ($rootScope, $scope, $hashService, $uibModal, $window, $odataresource, $http) {
        $window.scrollTo(0, 0);
        $scope.sortKey = 'cityId';
        $scope.reverse = false;
        $scope.num = 5;
        $scope.showData = true;
        $scope.showhistory = false;
        $scope.showAddCity = false;
        $scope.showEditCity = false;
        $scope.saving = false;

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        };

        // #endregion
        $scope.setView = function (index) {
            $scope.showData = false;
            $scope.showhistory = false;
            $scope.showAddCity = false;
            $scope.showEditCity = false;
            switch (index) {
                case 0:
                    $scope.showData = true;
                    break;
                case 1:
                    $scope.showhistory = true;
                    break;
                case 2:
                    $scope.showAddCity = true;
                    $scope.City = {
                        CityName: "",
                        isDeleted: false
                    }
                    $scope.cityform.$setPristine();
                    $scope.cityform.$setUntouched();
                    break;
                case 3:
                    $scope.showEditCity = true;
                    break;
                default:
            }
            $scope.allCities();
        }

        $scope.triggerFilter = function () {
            $scope.filter = $scope.filter ? false : true;
        }

        $scope.allCities = function () {
            $rootScope.showWait();
            $http.get('api/Cities').success(function (data) {
                $scope.city = data;
                $rootScope.citylist = [];
                for (var i = 0; i < $scope.city.length; i++) {
                    if ($scope.city[i].isDeleted === $scope.showhistory) {
                        $rootScope.citylist.push($scope.city[i]);
                    }
                }
                $rootScope.hideWait();
            }).error(function () {
                $rootScope.showError('Unable to load Data ', 'The System was Unable to bound Data From the Server .');
                $rootScope.hideWait();
            });
        }
        $scope.allCities();

        $scope.deleteCity = function (item) {
            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete city temporary from database ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';
            var confirmDeleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="ok()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    this City is related with patient you should delete patient frist \
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="ok()">OK</button>\
                </div>\
            </div>\
    </div>';
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

                    $scope.deletedItem = item;
                    var id = $scope.deletedItem.cityId;
                    $scope.delete = function () {
                        $scope.deletedItem.isDeleted = true;
                        $http.put('api/Cities/' + id, $scope.deletedItem).success(function (data) {
                            var index = $rootScope.citylist.indexOf($scope.deletedItem);
                            $rootScope.citylist.splice(index, 1);
                            $uibModalInstance.close();
                            $rootScope.showSuccess('The City Deleted', 'The city target was temporary deleted if you want to restore you can find it on Deleted Section.');
                            $hashService.postUserAction($rootScope.userId, 'delete an exsiting city');
                        }).error(function (data, status, headers, config) {
                            if (status === 405) {
                                $('.modal-content > .ng-scope').each(function () {
                                    try {
                                        $(this).scope().$dismiss();
                                    } catch (_) {
                                    }
                                });
                                var modalInstance = $uibModal.open({
                                    animation: $scope.animationsEnabled,
                                    template: confirmDeleteTemplete,
                                    controller: [
                                        '$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                            $scope.ok = function () {
                                                $uibModalInstance.close();
                                            };
                                        }
                                    ]
                                });
                                $rootScope.hideWait();
                            } else {
                                $rootScope.showSuccess('The can not Deleted', 'There is an error on the server.');
                            }
                        });
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };


        $scope.DeleteFinal = function (item) {
            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete This city permanently from database ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.deletedItem = item;
                    var id = $scope.deletedItem.cityId;
                    $scope.delete = function () {
                        $http.delete('api/Cities/' + id).success(function (data) {
                            var index = $rootScope.citylist.indexOf($scope.deletedItem);
                            $rootScope.citylist.splice(index, 1);
                            $uibModalInstance.close();
                            $hashService.postUserAction($rootScope.userId, 'delete city permanently');
                        }).error(function (data) {

                        });
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

        $scope.animationsEnabled = true;

        $scope.editCity = function (item) {
            $scope.setView(3);
            $scope.editItem = item;
        }
        $scope.savingEdit = false;
        $scope.saveEditCity = function () {
            $scope.savingEdit = true;

            $http.put('api/Cities/' + $scope.editItem.cityId, $scope.editItem).success(function (data) {
                $hashService.postUserAction($rootScope.userId, 'edit an existing city');
                $rootScope.showSuccess("success", 'Edit City done sucessfully');
                $scope.savingEdit = false;
            }).error(function (data) {
                $rootScope.showError("Error in city", 'An error happen while save city');
                $scope.savingEdit = false;
            });
        };

        $scope.restoreCity = function (item) {
            var restoreTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Restore</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Restoe This City ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="rest()">Restore</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: restoreTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.restoredItem = item;
                    var id = $scope.restoredItem.cityId;
                    $scope.rest = function () {
                        $scope.restoredItem.isDeleted = false;
                        $http.put('api/Cities/' + id, $scope.restoredItem).success(function (data) {
                            $hashService.postUserAction($rootScope.userId, 'restore an exist city');
                            var index = $rootScope.citylist.indexOf($scope.restoredItem);
                            $rootScope.citylist.splice(index, 1);
                            $uibModalInstance.close();
                        }).error(function (data) {
                        });
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

        $scope.City = {
            cityName: "",
            isDeleted: false
        }

        $scope.createCity = function () {
            $scope.saving = true;
            $http.post('api/Cities', $scope.City).success(function () {
                $hashService.postUserAction($rootScope.userId, 'Create new city');
                $rootScope.showSuccess("success", 'Add City done sucessfully');
                $scope.saving = false;
                $scope.setView(0);
            }).error(function (error) {
                $rootScope.showError('error in add city', 'There this an error while creating new city');
                $scope.saving = false;
            });
        }
    }]);

app.controller('allRolesCtrl', ['$rootScope', '$scope', '$hashService', '$uibModal', '$window', '$http',
    function ($rootScope, $scope, $hashService, $uibModal, $window, $http) {
        $window.scrollTo(0, 0);
        $scope.sortKey = 'Id';
        $scope.reverse = false;
        $scope.num = 5;

        $scope.showData = true;
        $scope.showhistory = false;
        $scope.showAddRole = false;
        $scope.showEditRole = false;

        $scope.AddPrivilages = [
      { Id: '', isAllowed: false, PrivilageId: 1 },
      { Id: '', isAllowed: false, PrivilageId: 2 },
      { Id: '', isAllowed: false, PrivilageId: 3 },
      { Id: '', isAllowed: false, PrivilageId: 4 },
      { Id: '', isAllowed: false, PrivilageId: 5 },
      { Id: '', isAllowed: false, PrivilageId: 6 },
      { Id: '', isAllowed: false, PrivilageId: 7 },
      { Id: '', isAllowed: false, PrivilageId: 8 },
      { Id: '', isAllowed: false, PrivilageId: 9 },
      { Id: '', isAllowed: false, PrivilageId: 10 },
      { Id: '', isAllowed: false, PrivilageId: 11 },
      { Id: '', isAllowed: false, PrivilageId: 12 },
      { Id: '', isAllowed: false, PrivilageId: 13 },
      { Id: '', isAllowed: false, PrivilageId: 14 },
      { Id: '', isAllowed: false, PrivilageId: 15 },
      { Id: '', isAllowed: false, PrivilageId: 16 },
      { Id: '', isAllowed: false, PrivilageId: 17 },
      { Id: '', isAllowed: false, PrivilageId: 18 },
      { Id: '', isAllowed: false, PrivilageId: 19 }
        ];
        $scope.EditPrivilages = [
     { Id: '', isAllowed: false, PrivilageId: 1 },
     { Id: '', isAllowed: false, PrivilageId: 2 },
     { Id: '', isAllowed: false, PrivilageId: 3 },
     { Id: '', isAllowed: false, PrivilageId: 4 },
     { Id: '', isAllowed: false, PrivilageId: 5 },
     { Id: '', isAllowed: false, PrivilageId: 6 },
     { Id: '', isAllowed: false, PrivilageId: 7 },
     { Id: '', isAllowed: false, PrivilageId: 8 },
     { Id: '', isAllowed: false, PrivilageId: 9 },
     { Id: '', isAllowed: false, PrivilageId: 10 },
     { Id: '', isAllowed: false, PrivilageId: 11 },
     { Id: '', isAllowed: false, PrivilageId: 12 },
     { Id: '', isAllowed: false, PrivilageId: 13 },
     { Id: '', isAllowed: false, PrivilageId: 14 },
     { Id: '', isAllowed: false, PrivilageId: 15 },
     { Id: '', isAllowed: false, PrivilageId: 16 },
     { Id: '', isAllowed: false, PrivilageId: 17 },
     { Id: '', isAllowed: false, PrivilageId: 18 },
     { Id: '', isAllowed: false, PrivilageId: 19 }
        ];

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        };

        $scope.triggerFilter = function () {
            $scope.filter = $scope.filter ? false : true;
        }

        $scope.setView = function (index) {
            $scope.showData = false;
            $scope.showhistory = false;
            $scope.showAddRole = false;
            $scope.showEditRole = false;
            switch (index) {
                case 0:
                    $scope.showData = true;
                    break;
                case 1:
                    $scope.showhistory = true;
                    break;
                case 2:
                    $scope.showAddRole = true;
                    break;
                case 3:
                    $scope.showEditRole = true;
                    break;
                default:
            }
            $scope.allRoles();
        }

        $scope.allRoles = function () {
            $rootScope.showWait();
            $http.get('api/ApplicationRoles').success(function (data) {
                $rootScope.rolelist = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDeleted === $scope.showhistory) {
                        $rootScope.rolelist.push(data[i]);
                    }
                }
                $rootScope.hideWait();
            }).error(function () {
                $rootScope.showError('Unable to load Data ', 'The System was Unable to bound Data From the Server .');
                $rootScope.hideWait();
            });
        }
        $scope.allRoles();

        var preventDeleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="ok()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    this Role is related with users you should delete user frist \
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="ok()">OK</button>\
                </div>\
            </div>\
    </div>';

        $scope.deleteRole = function (item) {
            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete This Role ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

                    $scope.deletedRole = item;

                    $scope.delete = function () {
                        $rootScope.showWait();
                        $scope.deletedRole.isDeleted = true;
                        $http.put('/api/ApplicationRoles/' + item.roleId, $scope.deletedRole).success(function (data) {
                            var index = $rootScope.rolelist.indexOf($scope.restoredRole);
                            $rootScope.rolelist.splice(index, 1);
                            $uibModalInstance.close();
                            $rootScope.showSuccess('The User Group Deleted', 'The User target group was temporary deleted if you want to restore you can find it on Deleted Section.');
                            $hashService.postUserAction($rootScope.userId, 'Delete an existing Role');
                            $rootScope.hideWait();
                        }).error(function (data, status, headers, config) {
                            if (status === 405) {
                                $('.modal-content > .ng-scope').each(function () {
                                    try {
                                        $(this).scope().$dismiss();
                                    } catch (_) {
                                    }
                                });
                                var modalInstance = $uibModal.open({
                                    animation: $scope.animationsEnabled,
                                    template: preventDeleteTemplete,
                                    controller: [
                                        '$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                            $scope.ok = function () {
                                                $uibModalInstance.close();
                                            };
                                        }
                                    ]
                                });
                                $rootScope.hideWait();
                            } else {
                                $rootScope.showSuccess('The Groub can not Deleted', 'There is an exception on the server.');
                                $rootScope.hideWait();
                            }
                        });
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

        $scope.DeleteRFinal = function (item) {
            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete This Role ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.deletedRole = item;
                    console.log(item);
                    var id = $scope.deletedRole.roleId;

                    $scope.delete = function () {
                        $rootScope.showWait();
                        $http.delete('api/ApplicationRoles/DeleteRole/' + id).success(function (data) {
                            var index = $rootScope.rolelist.indexOf($scope.deletedRole);
                            $rootScope.rolelist.splice(index, 1);
                            $uibModalInstance.close();
                            $rootScope.hideWait();
                            $rootScope.showSuccess('Group Was Deleted Premently', 'the group was trunkated and out of Database.');
                            $hashService.postUserAction($rootScope.userId, 'Delete an existing Role');
                        }).error(function (data, status) {
                            $rootScope.showError('Cant Delete The Group', 'the group has related Data still in the database so the system was unable to Remove it.');
                        });
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });

            $scope.setView(0);
        };

        $scope.animationsEnabled = true;

        $scope.restoreRole = function (item) {
            var restoreTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Restore</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Restoe This Role ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="rest()">Restore</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: restoreTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.restoredRole = item;
                    var id = $scope.restoredRole.roleId;
                    $scope.rest = function () {
                        $scope.restoredRole.isDeleted = false;
                        $rootScope.showWait();
                        $http.put('/api/ApplicationRoles/' + id, $scope.restoredRole).success(function (data) {
                            var index = $rootScope.rolelist.indexOf($scope.restoredRole);
                            $rootScope.rolelist.splice(index, 1);
                            $uibModalInstance.close();
                            $rootScope.hideWait();
                            $rootScope.showSuccess('Group Restored Sucessfully', 'the target Group Was Restored to the original Data You can found it in the all data section .');
                            $hashService.postUserAction($rootScope.userId, 'Restore an existing Role');
                        }).error(function (data) {
                            $uibModalInstance.close();
                            $rootScope.hideWait();
                            $rootScope.showError('Cant Delete The Group', 'Cant Restore The Item , the System Faces Some Problem Restoring The Data.');
                        });
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
            $scope.setView(0);
        };

        $scope.newRole = {
            RoleName: '',
            RolePrivilages: {}
        };
        $scope.savingAddRole = false;
        $scope.addRole = function () {
            $scope.savingAddRole = true;
            $rootScope.showWait();
            if ($scope.RoleName && $scope.IsPrivilageValid($scope.AddPrivilages)) {
                $scope.newRole.RolePrivilages = $scope.AddPrivilages;
                $scope.newRole.RoleName = $scope.RoleName;
                $http.post('api/ApplicationRoles', $scope.newRole).success(function (response) {
                    $rootScope.showSuccess('Role has been saved', 'New Role has been registered ');
                    $scope.savingAddRole = false;
                    $scope.setView(0);
                    $scope.RoleName = '';
                    $scope.AddPrivilages = [
                                { Id: '', isAllowed: false, PrivilageId: 1 },
                                { Id: '', isAllowed: false, PrivilageId: 2 },
                                { Id: '', isAllowed: false, PrivilageId: 3 },
                                { Id: '', isAllowed: false, PrivilageId: 4 },
                                { Id: '', isAllowed: false, PrivilageId: 5 },
                                { Id: '', isAllowed: false, PrivilageId: 6 },
                                { Id: '', isAllowed: false, PrivilageId: 7 },
                                { Id: '', isAllowed: false, PrivilageId: 8 },
                                { Id: '', isAllowed: false, PrivilageId: 9 },
                                { Id: '', isAllowed: false, PrivilageId: 10 },
                                { Id: '', isAllowed: false, PrivilageId: 11 },
                                { Id: '', isAllowed: false, PrivilageId: 12 },
                                { Id: '', isAllowed: false, PrivilageId: 13 },
                                { Id: '', isAllowed: false, PrivilageId: 14 },
                                { Id: '', isAllowed: false, PrivilageId: 15 },
                                { Id: '', isAllowed: false, PrivilageId: 16 },
                                { Id: '', isAllowed: false, PrivilageId: 17 }
                    ];
                    $rootScope.hideWait();
                    $rootScope.showSuccess('the Role has been added', 'the Role was added Successfully to the Database.. ');
                    $hashService.postUserAction($rootScope.userId, 'Add a new role');
                }).error(function (error, status) {
                    $scope.savingAddRole = false;
                    if (status === 403)
                        $rootScope.showError('this Role Exist', 'a Role With The Same name Was Found on The Database .. ');
                    else
                        $rootScope.showError('Cant Add Role', 'the System Faces Some Complication Inserting New Role To The DataBase.. ');
                });
            }
            else {
                $scope.savingAddRole = false;
                $rootScope.showError('Role is Not Valid', 'Please Complete the Data Required to register role ');
                $rootScope.hideWait();
            }
        }

        $scope.editRole = function (item) {
            $rootScope.showWait();
            $scope.setView(3);
            $scope.editedRole = item;
            $http.get('api/RolePrivilages/' + item.roleId).success(function (data) {
                $scope.EditPrivilages = data;
                $rootScope.hideWait();
            });
        }

        $scope.IsPrivilageValid = function (Privilages) {
            var isvalid = false;
            for (var i = 0; i < Privilages.length; i++) {
                if (Privilages[i].isAllowed) {
                    isvalid = true;
                    break;
                }
            }
            return isvalid;
        };

        $scope.savingEditRole = false;
        $scope.saveEdit = function () {
            $scope.savingEditRole = true;
            $rootScope.showWait();
            if ($scope.editedRole.roleName && $scope.IsPrivilageValid($scope.EditPrivilages)) {
                $scope.roleUser = {
                    RoleId: $scope.editedRole.roleId,
                    RoleName: $scope.editedRole.roleName,
                    RolePrivilages: {}
                };
                $scope.roleUser.RolePrivilages = $scope.EditPrivilages;
                $http.put('api/ApplicationRoles/' + $scope.roleUser.RoleId, $scope.roleUser).success(function (data) {
                    $rootScope.showSuccess('Role Edit Done', 'The Role has Been Saved Successfully .');
                    $scope.savingEditRole = false;
                    $scope.setView(0);
                    $scope.editedRole = null;
                    $rootScope.hideWait();
                    $hashService.postUserAction($rootScope.userId, 'Edit an existing role');
                });
            }
            else {
                $rootScope.showError('Role is Not Valid', 'Please Complete the Data Required to register role ');
                $rootScope.hideWait();
            }
        };

        $scope.logRole = function () {
            $http.get('api/RolePrivilages/' + $scope.selectedRole).success(function (data) {
                $scope.Privilages = data;
            });
            $http.get('api/ApplicationRoles/' + $scope.selectedRole).success(function (data) {
                $scope.editedRole = {
                    roleName: data.name
                }
            });
        }

    }]);

app.controller('allUsersCtrl', ['$http', '$rootScope', '$scope', '$timeout', '$hashService', '$uibModal', '$window',
    function ($http, $rootScope, $scope, $timeout, $hashService, $uibModal, $window) {
        $window.scrollTo(0, 0);
        $scope.sortKey = 'Id';
        $scope.reverse = false;
        $scope.num = 5;
        $scope.showData = true;
        $scope.showhistory = false;
        $scope.AddPrivilages = [
       { Id: '', isAllowed: false, PrivilageId: 1 },
       { Id: '', isAllowed: false, PrivilageId: 2 },
       { Id: '', isAllowed: false, PrivilageId: 3 },
       { Id: '', isAllowed: false, PrivilageId: 4 },
       { Id: '', isAllowed: false, PrivilageId: 5 },
       { Id: '', isAllowed: false, PrivilageId: 6 },
       { Id: '', isAllowed: false, PrivilageId: 7 },
       { Id: '', isAllowed: false, PrivilageId: 8 },
       { Id: '', isAllowed: false, PrivilageId: 9 },
       { Id: '', isAllowed: false, PrivilageId: 10 },
       { Id: '', isAllowed: false, PrivilageId: 11 },
       { Id: '', isAllowed: false, PrivilageId: 12 },
       { Id: '', isAllowed: false, PrivilageId: 13 },
       { Id: '', isAllowed: false, PrivilageId: 14 },
       { Id: '', isAllowed: false, PrivilageId: 15 },
       { Id: '', isAllowed: false, PrivilageId: 16 },
       { Id: '', isAllowed: false, PrivilageId: 17 },
       { Id: '', isAllowed: false, PrivilageId: 18 },
       { Id: '', isAllowed: false, PrivilageId: 19 }];
        $scope.EditPrivilages = [
       { Id: '', isAllowed: false, PrivilageId: 1 },
       { Id: '', isAllowed: false, PrivilageId: 2 },
       { Id: '', isAllowed: false, PrivilageId: 3 },
       { Id: '', isAllowed: false, PrivilageId: 4 },
       { Id: '', isAllowed: false, PrivilageId: 5 },
       { Id: '', isAllowed: false, PrivilageId: 6 },
       { Id: '', isAllowed: false, PrivilageId: 7 },
       { Id: '', isAllowed: false, PrivilageId: 8 },
       { Id: '', isAllowed: false, PrivilageId: 9 },
       { Id: '', isAllowed: false, PrivilageId: 10 },
       { Id: '', isAllowed: false, PrivilageId: 11 },
       { Id: '', isAllowed: false, PrivilageId: 12 },
       { Id: '', isAllowed: false, PrivilageId: 13 },
       { Id: '', isAllowed: false, PrivilageId: 14 },
       { Id: '', isAllowed: false, PrivilageId: 15 },
       { Id: '', isAllowed: false, PrivilageId: 16 },
       { Id: '', isAllowed: false, PrivilageId: 17 },
       { Id: '', isAllowed: false, PrivilageId: 18 },
       { Id: '', isAllowed: false, PrivilageId: 19 }
        ];
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        };

        $scope.logPrivilage = function () {
            $http.get('api/RolePrivilages/' + $scope.registration.RoleId).success(function (data) {
                $scope.AddPrivilages = data;
            });
        }
        $scope.logEditPrivilage = function () {
            $http.get('api/RolePrivilages/' + $scope.editedUser.roleId).success(function (data) {
                $scope.EditPrivilages = data;
            });
        }
        $scope.generateName = function () {
            $scope.registration.UserName = generate_name('egyptian');
        }
        $scope.generatPassword = function () {
            $scope.registration.Password = generat_password();
        }
        $scope.triggerFilter = function () {
            $scope.filter = $scope.filter ? false : true;
        }
        $scope.setView = function (index) {
            $scope.showData = false;
            $scope.showhistory = false;
            $scope.showAdd = false;
            $scope.showEdit = false;
            switch (index) {
                case 0:
                    $scope.showData = true;
                    break;
                case 1:
                    $scope.showhistory = true;
                    break;
                case 2:
                    $scope.showAdd = true;
                    $scope.registration = {
                        FullName: "",
                        UserName: "",
                        Password: "",
                        RoleId: "",
                        UserPrivilages: {}
                    };
                    $scope.AddPrivilages = [];
                    $scope.userform.$setPristine();
                    $scope.userform.$setUntouched();
                    break;
                case 3:
                    $scope.showEdit = true;
                    break;
                default:
            }
            $scope.allUsers();
            //$scope.allRoles();
        }
        $scope.allUsers = function () {
            $rootScope.showWait();
            $http.get('api/ApplicationUsers/GetApplicationUsers').success(function (data) {
                $rootScope.userlist = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDeleted === $scope.showhistory) {
                        $rootScope.userlist.push(data[i]);
                    }
                }
                $rootScope.hideWait();
            }).error(function () {
                $rootScope.showError('Unable to load Data ', 'The System was Unable to bound Data From the Server .');
                $rootScope.hideWait();
            });
        };
        $scope.allUsers();

        $scope.allRoles = function () {
            $http.get('api/ApplicationRoles').success(function (data) {
                $rootScope.rolelist = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDeleted === false) {
                        $rootScope.rolelist.push(data[i]);
                    }
                }
            });
        }
        $scope.allRoles();

        $scope.allCities = function () {
            $rootScope.showWait();
            $http.get('api/Cities').success(function (data) {
                $scope.cityList = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDeleted === false) {
                        $scope.cityList.push(data[i]);
                    }
                }
                $rootScope.hideWait();
            }).error(function () {
                $rootScope.showError('Unable to load Data ', 'The System was Unable to bound Data From the Server .');
                $rootScope.hideWait();
            });
        }
        $scope.allCities();

        $scope.registration = {
            FullName: "",
            UserName: "",
            Password: "",
            RoleId: "",
            CityId: "",
            Email: "",
            MobileNumber: "",
            JobTitle: "",
            HealthFacility: "",
            UserPrivilages: {}
        };
        $scope.savingsignUp = false;
        $scope.signUp = function () {

            $scope.savingsignUp = true;
            $scope.registration.UserPrivilages = $scope.AddPrivilages;
            if ($scope.registration.Password && $scope.registration.UserName && $scope.registration.RoleId
                && $scope.registration.CityId && $scope.registration.Email && $scope.registration.MobileNumber
                && $scope.registration.JobTitle && $scope.registration.HealthFacility) {

                $http.post('/account/register', $scope.registration).success(function (response) {
                    $hashService.postUserAction($rootScope.userId, 'Add a new User');
                    if (response.success) {
                        $rootScope.showSuccess('success Register User', 'add user operation done successfully');
                        $scope.savingsignUp = false;
                        $scope.setView(0);
                    }
                    else {
                        $rootScope.showError('Cant Register User', ' The User Name is already Exists.');
                        $scope.savingsignUp = false;
                    }
                });

            } else {
                $rootScope.showError('Cant Creeate User', 'There Is Some Field Required to be filled ');
                $scope.savingsignUp = false;
            }
        };

        $scope.deleteUser = function (item) {

            var preventDeleteUser = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="ok()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    You cannot delete this user beacause he create patient previous \
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="ok()">OK</button>\
                </div>\
            </div>\
    </div>';

            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete This user ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.deletedItem = item;

                    $http.get('api/ApplicationUsers/' + item.id).success(function (data) {
                        $scope.actualItem = data;
                        $scope.delete = function () {
                            $scope.actualItem.isDeleted = true;
                            $http.put('api/ApplicationUsers/' + item.id, $scope.actualItem).success(function (response) {
                                var index = $rootScope.userlist.indexOf($scope.deletedItem);
                                $rootScope.userlist.splice(index, 1);
                                $uibModalInstance.close();
                                $hashService.postUserAction($rootScope.userId, 'Neglect an existing user');
                                $rootScope.showSuccess('Delete user', 'Delete User operation is done successfully');
                            }).error(function (data, status) {
                                if (status === 405) {
                                    $('.modal-content > .ng-scope').each(function () {
                                        try {
                                            $(this).scope().$dismiss();
                                        } catch (_) {
                                        }
                                    });
                                    var modalInstance = $uibModal.open({
                                        animation: $scope.animationsEnabled,
                                        template: preventDeleteUser,
                                        controller: [
                                            '$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                                $scope.ok = function () {
                                                    $uibModalInstance.close();
                                                };
                                            }
                                        ]
                                    });
                                    $rootScope.hideWait();
                                } else {
                                    $rootScope.showSuccess('The User can not Deleted', 'There is an exception on the server.');
                                    $rootScope.hideWait();
                                }
                            });
                        };
                    });
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

        $scope.DeleteUFinal = function (item) {
            var deleteTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Delete This user ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="delete()">Delete</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: deleteTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.deletedItem = item;

                    $scope.delete = function () {
                        $http.delete('api/ApplicationUsers/' + item.id).then(function (response) {
                            var index = $rootScope.userlist.indexOf($scope.deletedItem);
                            $rootScope.userlist.splice(index, 1);
                            $uibModalInstance.close();
                            $hashService.postUserAction($rootScope.userId, 'Delete an existing user');
                            $rootScope.showSuccess('Element Deleted  Premently ', 'The Target Element is deleted premently form the system you should now that you cant restore it again . ');
                        }, function (response) {
                            $rootScope.showError('Cant Delete Item  ', 'It seems that the target Element is linked with one or more Entity so the system cant delete it  ')
                        });
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        };

        $scope.restoreUser = function (item) {
            var restoreTemplete = '<div>\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" ng-click="cancel()">\
                        <p aria-hidden="true">&times;</p>\
                    </button>\
                    <h4 class="modal-title" id="myModalLabel2">Restore</h4>\
                </div>\
                <div class="modal-body">\
                    Do You Want To Restoe This User ?\
                </div>\
                <div class="modal-footer">\
                    <button type="submit" class="btn btn-success btnConfirm" ng-click="rest()">Restore</button>\
                    <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>\
                </div>\
            </div>\
    </div>';
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                template: restoreTemplete,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.restoredUser = item;

                    $http.get('api/ApplicationUsers/' + item.id).success(function (data) {
                        $scope.actualItem = data;


                        $scope.rest = function () {
                            $scope.actualItem.isDeleted = false;
                            $http.put('api/ApplicationUsers/' + item.id, $scope.actualItem).then(function (response) {
                                var index = $rootScope.userlist.indexOf($scope.deletedItem);
                                $rootScope.userlist.splice(index, 1);
                                $uibModalInstance.close();
                                $rootScope.showSuccess('Item Restored Successfuly  ', 'the Target ELement is Now in your List You can Manage it  ');
                                $hashService.postUserAction($rootScope.userId, 'Restore an existing user');
                            }, function (response) {
                                $rootScope.showError('Cant restore Element  ', 'the application cant restore this item if you see thismessage more please contact support .')
                            });

                            $scope.cancel = function () {
                                $uibModalInstance.close();
                            };
                        }
                    });
                }]
            });
        };

        $scope.editUser = function (item) {
            $scope.setView(3);
            $scope.editedUser = item;
            $http.get('api/UserPrivilages/' + item.id).success(function (data) {
                $scope.EditPrivilages = data;
            });
        }

        $scope.savingedit = false;
        $scope.saveEdit = function () {
            $scope.readyUser = {
                Id: $scope.editedUser.id,
                RoleId: $scope.editedUser.roleId,
                UserName: $scope.editedUser.userName,
                FullName: $scope.editedUser.fullName,
                CityId: $scope.editedUser.cityId,
                Email: $scope.editedUser.email,
                MobileNumber: $scope.editedUser.mobileNumber,
                JobTitle: $scope.editedUser.jobTitle,
                HealthFacility: $scope.editedUser.healthFacility,
                UserPrivilages: {}
            };
            //$rootScope.showWait();
            $scope.savingedit = true;
            if ($scope.selectedUser || ($scope.editedUser.roleId && $scope.editedUser.id)) {

                if ($scope.readyUser.UserName && $scope.readyUser.RoleId
                    && $scope.readyUser.CityId && $scope.readyUser.Email && $scope.readyUser.MobileNumber
                    && $scope.readyUser.JobTitle && $scope.readyUser.HealthFacility) {
                    $scope.readyUser.FullName = $scope.editedUser.fullName;
                    $scope.editedUser.UserPrivilages = $scope.EditPrivilages;
                    $http.put('api/ApplicationUsers/' + $scope.readyUser.Id, $scope.editedUser).success(function (data) {
                        $hashService.postUserAction($rootScope.userId, 'Edit an existing user');
                        $scope.savingedit = false;
                        $rootScope.showSuccess('User Saved Successfully', 'Successfully saved the user state and Privilages .');
                        //$rootScope.showHide();
                    }).error(function (data) {
                        $rootScope.showError('Cant Save User Data', 'there is some Problems in updating User Data .');
                        //$rootScope.showHide();
                        $scope.savingedit = false;
                    });
                }
                else {
                    $rootScope.showError('Cant Update User', 'There Is Some Field Required to be filled ');
                    // $rootScope.showHide();
                    $scope.savingedit = false;
                }
            }
            else {
                $rootScope.showError('Cant Update User', 'There Is Some Field Required to be filled ');
                $rootScope.showHide();
                $scope.savingedit = false;
            }
        };

        $scope.logUser = function () {
            $http.get('api/UserPrivilages/' + $scope.selectedUser).success(function (data) {
                $scope.EditPrivilages = data;
            });
            $http.get('api/ApplicationUsers/' + $scope.selectedUser).success(function (data) {
                $scope.editedUser = data;
            });
        };
    }]);

app.controller('reportCtrl', ['$scope', '$rootScope', '$window', '$hashService', '$http',
     function ($scope, $rootScope, $window, $hashService, $http) {
         $window.scrollTo(0, 0);
         $scope.showResult = false;
         $scope.showPatientInfo = true;
         $scope.showriskFactor = false;
         $scope.showdiagnoses = false;
         $scope.showosteoporosis = false;
         $scope.setView = function (index) {
             $scope.showPatientInfo = false;
             $scope.showriskFactor = false;
             $scope.showdiagnoses = false;
             $scope.showosteoporosis = false;
             switch (index) {
                 case 0:
                     $scope.showPatientInfo = true;
                     break;
                 case 1:
                     $scope.showriskFactor = true;
                     break;
                 case 2:
                     $scope.showdiagnoses = true;
                     break;
                 case 3:
                     $scope.showosteoporosis = true;
                     break;
                 default:
             }
         }

         $scope.allCities = function () {
             $rootScope.showWait();
             $http.get('api/Cities').success(function (data) {
                 $scope.cities = [];

                 for (var i = 0; i < data.length; i++) {
                     if (data[i].isDeleted === true) {
                         $scope.cities.push(data[i]);
                     }
                 }
                 $rootScope.citylist = [];
                 $rootScope.hideWait();
             }).error(function () {
                 $rootScope.showError('Unable to load Data ', 'The System was Unable to bound Data From the Server .');
                 $rootScope.hideWait();
             });
         }
         $scope.allCities();

         $scope.patient = {
             BodyMassIndex: '',
             PatientGender: '',
             HasFracture: '',
             Education: '',
             OsteoporosisCase: '',
             Occupation: ''
         };

         $scope.fracture = '';

         $scope.Occupation = '';
         $scope.occupationArr = [
             'Unemployed', 'Student', 'Professional', 'Manual_Labor', 'Unknown', 'Other_Specify'
         ];
         $scope.Education = '';
         $scope.EducationArr = ['Un_educated', 'Read_Write', 'Primary', 'Intermediate', 'Secondary', 'University'];

         $scope.OsteoporosisCase = '';
         $scope.caseStatusArr = ['OsteoporosisWithFracture', 'Osteoporosis', 'Osteopenia', 'Normal'];

         $scope.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

         $scope.risk = '';
         $scope.riskModel = {
             LowVitamin: false,
             HormoneTherapy: false,
             Smoking: false,
             RheumatoidArthritis: false,
             Glucocorticoid: false,
             SecondryCauses: false,
             PersonalHistoryOfFragilityFracture: false,
             FamilyHistoryOfOsteoporoticFracture: false,
             LowCalciumIntakelesThanTwoServings: false
         };

         // dates 
         $scope.dateOptions = {
             formatYear: 'yy',
             maxDate: new Date(),
             minDate: new Date(1800, 12, 30),
             startingDay: 1
         };

         $scope.dbpTestF = {
             opened: false
         };
         $scope.openTF = function () {
             $scope.dbpTestF.opened = true;
         };
         $scope.format = 'dd/MM/yyyy';
         $scope.altInputFormats = ['M!/d!/yyyy'];

         $scope.todayTestF = function () {
             $scope.TestDateFrom = new Date();
         };
         $scope.todayTestF();

         $scope.clearTestF = function () {
             $scope.TestDateFrom = null;
         };
         //--------------- TestDateTo -------------
         $scope.dateOptions = {
             formatYear: 'yy',
             maxDate: new Date(),
             minDate: new Date(1800, 12, 30),
             startingDay: 1
         };

         $scope.dbpTestT = {
             opened: false
         };
         $scope.openTT = function () {
             $scope.dbpTestT.opened = true;
         };
         $scope.format = 'dd/MM/yyyy';
         $scope.altInputFormats = ['M!/d!/yyyy'];

         $scope.todayTestT = function () {
             $scope.TestDateTo = new Date();
         };
         $scope.todayTestT();

         $scope.clearTestT = function () {
             $scope.TestDateTo = null;
         };
         //--------------- BirthDateFrom -------------
         $scope.bdateOptions = {
             formatYear: 'yy',
             maxDate: new Date(),
             minDate: new Date(1800, 12, 30),
             startingDay: 1
         };

         $scope.dbpBirthF = {
             opened: false
         };
         $scope.openBF = function () {
             $scope.dbpBirthF.opened = true;
         };
         $scope.format = 'dd/MM/yyyy';
         $scope.altInputFormats = ['M!/d!/yyyy'];

         $scope.todayBirthF = function () {
             $scope.TestBirthFrom = new Date();
         };
         $scope.todayBirthF();

         $scope.clearBirthF = function () {
             $scope.TestBirthFrom = null;
         };

         //--------------- BirthDateTo -------------
         $scope.bdateOptions = {
             formatYear: 'yy',
             maxDate: new Date(),
             minDate: new Date(1800, 12, 30),
             startingDay: 1
         };

         $scope.dbpBirthT = {
             opened: false
         };
         $scope.openBT = function () {
             $scope.dbpBirthT.opened = true;
         };
         $scope.format = 'dd/MM/yyyy';
         $scope.altInputFormats = ['M!/d!/yyyy'];

         $scope.todayBirthT = function () {
             $scope.TestBirthTo = new Date();
         };
         $scope.todayBirthT();

         $scope.clearBirthT = function () {
             $scope.TestBirthTo = null;
         };

         // shared on date
         function getDayClass(data) {
             var date = data.date,
               mode = data.mode;
             if (mode === 'day') {
                 var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                 for (var i = 0; i < $scope.events.length; i++) {
                     var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                     if (dayToCheck === currentDay) {
                         return $scope.events[i].status;
                     }
                 }
             }
             return '';
         }

         $scope.PrintReport = function (item) {
             var url = '/Main/PatientReport/' + item.Id;
             var win = window.open(url, '_blank');
             win.focus();
         };

         $scope.blobToFile = function (theBlob, fileName) {
             //A Blob() is almost a File() - it's just missing the two properties below which we will add
             theBlob.lastModifiedDate = new Date();
             theBlob.name = fileName;
             return theBlob;
         }

         $scope.checkResults = [];

         $scope.printSummaryResult = function (fileType) {
             angular.forEach($scope.riskModel, function (value, key) {
                 if (value) {
                     $scope.checkResults.push(key);
                 }
             });
             var checkStr = $scope.checkResults.toString();
             $scope.patient.BodyMassIndex = checkStr;
             $scope.patient.FileType = fileType;
             $scope.patient.Occupation = $scope.occupationArr.indexOf($scope.Occupation);
             $scope.patient.Education = $scope.EducationArr.indexOf($scope.Education);
             $scope.patient.OsteoporosisCase = $scope.caseStatusArr.indexOf($scope.OsteoporosisCase);

             $http.post('Main/FilterPatientReport', $scope.patient, { responseType: 'arraybuffer' })
                      .success(function () {
                          var url = '/Main/ExportFile/';
                          var win = window.open(url, '_blank');
                          win.focus();
                      }).error(function () {
                          $rootScope.showError('Report Error', 'You must enter some data tofilter on it');
                      });
         };


         $scope.riskResults = [];
         $scope.reporting = false;
         $scope.loading = false;
         $scope.showAllResult = function () {
             $scope.reporting = true;
             $scope.loading = true;
             $scope.showResult = false;
             angular.forEach($scope.riskModel, function (value, key) {
                 if (value) {
                     $scope.riskResults.push(key);
                 }
             });

             var checkStr = $scope.riskResults.toString();
             $scope.patient.BodyMassIndex = checkStr;
             $scope.patient.Occupation = $scope.occupationArr.indexOf($scope.Occupation);
             $scope.patient.Education = $scope.EducationArr.indexOf($scope.Education);
             $scope.patient.OsteoporosisCase = $scope.caseStatusArr.indexOf($scope.OsteoporosisCase);

             if ($scope.patient == null) {
                 $rootScope.showError('Report Error', 'You must enter some data tofilter on it');
             }

             $http.post('/Main/ShowReportResult', $scope.patient).success(function (data) {
                 $scope.list = data;
                 $scope.reporting = false;
                 $scope.loading = false;
                 $scope.showResult = true;
                 $scope.riskResults = [];
             }).error(function () {
                 $scope.reporting = false;
                 $scope.loading = false;
                 $rootScope.showError('Server Error', 'You Check your internet connection');
             });
         }

         $scope.printAllResult = function () {
             angular.forEach($scope.riskModel, function (value, key) {
                 if (value) {
                     $scope.checkResults.push(key);
                 }
             });
             var checkStr = $scope.checkResults.toString();
             $scope.patient.BodyMassIndex = checkStr;
             $scope.patient.Occupation = $scope.occupationArr.indexOf($scope.Occupation);
             $scope.patient.Education = $scope.EducationArr.indexOf($scope.Education);
             $scope.patient.OsteoporosisCase = $scope.caseStatusArr.indexOf($scope.OsteoporosisCase);

             $http.post('Main/PrintReportResult', $scope.patient).success(function (response) {
                 var file = new Blob([response], { type: 'application/pdf' });
                 var fileURL = URL.createObjectURL(file);
                 window.open(fileURL);
             }).error(function () {
                 $rootScope.showError('Report Error', 'You must enter some data tofilter on it');
             });
         }

     }]);

app.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});

app.directive('kformInput', function ($compile) {
    return {
        restrict: 'E',
        link: function ($scope, $element, $attrs) {
            var isdisabled = '';
            if ($attrs.hasOwnProperty('disabled')) {
                isdisabled = 'disabled';
            }
            var addonClass = '';
            if ($attrs.hasOwnProperty('addonClass')) {
                addonClass = $attrs.addonClass;
            }
            var addonMethod = '';
            if ($attrs.hasOwnProperty('addonMethod')) {
                addonMethod = $attrs.addonMethod;
            }
            var addonTooltip = '';
            if ($attrs.hasOwnProperty('addonTooltip')) {
                addonTooltip = $attrs.addonTooltip;
            }
            var deleteicon = '';
            if ($attrs.hasOwnProperty('deleteicon')) {
                deleteicon = $attrs.deleteicon;
            }
            var squt = "'";
            var temp = '<div class="form-group" >\
                       <div class="input-group">\
                       <span class="input-group-addon" id="' + $attrs.id + '"> <i class="fa fa-angle-double-right"></i> ' + $attrs.diplaytxt + ' : </span>\
                       <input aria-describedby="' + $attrs.id + '" type="' + $attrs.type + '"name="' + $attrs.name + '" class="form-control"' + isdisabled + ' ng-model="' + $attrs.modelname + '" placeholder=" Enter ' + $attrs.diplaytxt + '" uib-tooltip="This Field Is Required" tooltip-placement="bottom"   tooltip-enable="!' + $attrs.modelname + '" required>';
            if (addonMethod && addonClass && addonTooltip) {
                temp += ' <span class="input-group-btn" >\
                        <button style="border-radius:0 0 0 0;"  ng-click="' + addonMethod + '" uib-tooltip="' + addonTooltip + '" tooltip-placement="bottom"   tooltip-enable="true" class="btn btn-default">\
                            <span class="' + addonClass + '"></span>\
                        </button>\
                      </span>';
            }
            if (deleteicon === 'true') {
                temp += ' <span class="input-group-btn">\
                        <button ng-click="' + $attrs.modelname + ' = undefined;"  uib-tooltip="Clear Text Value" tooltip-placement="bottom"   tooltip-enable="true"  class="btn btn-default">\
                            <span class="glyphicon glyphicon-trash"></span>\
                        </button>\
                      </span>';
                temp += '</div>';
            }

            temp += ' </div>';

            var formgroupTemplate = angular.element(temp);
            var template = $compile(formgroupTemplate)($scope);
            $element.replaceWith(template);
        }
    };
});

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 8);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

//-------- Map Directive -----------------
app.directive('svgMap', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        templateUrl: 'img/saudiArabiaHigh.svg',
        link: function (scope, element) {
            var regions = element[0].querySelectorAll('.state');
            angular.forEach(regions, function (path) {
                var regionElement = angular.element(path);
                regionElement.attr("region", "");
                regionElement.attr("dummy-data", "dummyData");
                regionElement.attr("hover-region", "hoverRegion");
                $compile(regionElement)(scope);
            });
        }
    }
}]);

app.directive('region', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        scope: {
            dummyData: "=",
            hoverRegion: "="
        },
        link: function (scope, element, attrs) {
            scope.elementId = element.attr("id");
            scope.regionClick = function () {
                //alert(scope.dummyData[scope.elementId].value);
            };
            scope.regionMouseOver = function () {
                scope.hoverRegion = scope.elementId;
                element[0].parentNode.appendChild(element[0]);
            };
            element.attr("ng-click", "regionClick()");
            element.attr("ng-attr-fill", "{{dummyData[elementId].value | map_colour}}");
            element.attr("ng-mouseover", "regionMouseOver()");
            element.attr("ng-class", "{active:hoverRegion==elementId}");
            element.removeAttr("region");
            $compile(element)(scope);
        }
    }
}]);

app.filter('map_colour', [function () {
    return function (input) {
        var b = 255 - Math.floor(input * 255);
        var g = Math.floor(input * 255);
        return "rgba(255," + g + "," + b + ",1)";
    }
}]);