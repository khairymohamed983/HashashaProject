﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HashashaSPA.Models
{
    // Models returned by AccountController actions.
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Hometown")]
        public string Hometown { get; set; }
    }

    public class UserActionModel
    {
        public string ActionDescription { get; set; }
    }
    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "Write user full name")]
        public string FullName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Choose User Name to login with it")]
        public string UserName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public int CityId { get; set; }
        public string MobileNumber { get; set; }
        public string JobTitle { get; set; }
        //public double HealthFacility { get; set; }
        public string HealthFacility { get; set; }
        public List<UserPrivilageViewModel> UserPrivilages { get; set; }



    }

    public class UserPrivilageViewModel
    {
        public string Id { get; set; }
        public int PrivilageId { get; set; }
        public bool isAllowed { get; set; }
    }

    public class EditUserViewModel
    {
        public string Id { get; set; }
        public string RoleId { get; set; }
        public bool isDeleted { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string LastLogin { get; set; }
        public string CreateDate { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int CityId { get; set; }
        public string MobileNumber { get; set; }
        public string JobTitle { get; set; }
        //public double HealthFacility { get; set; }
        public string HealthFacility { get; set; }
        public List<UserPrivilageViewModel> UserPrivilages { get; set; }
    }

    public class UserViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public bool isDeleted { get; set; }
        public string RoleName { get; set; }
        public string RoleId { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime CreateDate { get; set; }
        //public string Password { get; set; }
        public string Email { get; set; }
        public int CityId { get; set; }
        public string MobileNumber { get; set; }
        public string JobTitle { get; set; }
        //public double HealthFacility { get; set; }
        public string HealthFacility { get; set; }
        public List<UserViewPrivilage> UserPrivilages { get; set; }

    }
    public class UserViewPrivilage
    {
        public int PrivilageId { get; set; }
        public bool isAllowed { get; set; }
    }


    public class AddRoleViewModel
    {
        public string RoleName { get; set; }
        public bool isDeleted { get; set; }
        public List<UserViewPrivilage> RolePrivilages { get; set; }
    }

    public class RolePrivilageViewModel
    {
        public int PrivilageId { get; set; }
        public string PrivilageName { get; set; }
        public bool isAllowed { get; set; }
    }

    public class RoleViewModel
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool isDeleted { get; set; }
        public List<RolePrivilageViewModel> RolePrivilages { get; set; }
    }

    public class EditRoleViewModel
    {
        public string Id { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool isDeleted { get; set; }
        public ICollection<RolePrivilage> RolePrivilages { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ConnterViewModel
    {
        public int PatientsNum { get; set; }
        public int RolesNum { get; set; }
        public int UsersNum { get; set; }
        public int CitiesNum { get; set; }
    }


    public class FilterPatientViewModel
    {
        public int? PatientId { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientFatherName { get; set; }
        public string PatientLastName { get; set; }
        public bool? PatientGender { get; set; }
        public string NationalId { get; set; }
        public DateTime? TestDateFrom { get; set; }
        public DateTime? TestDateTo { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public DateTime? CreationDate { get; set; }
        public string ContactNumber { get; set; }
        public string MobileNumber { get; set; }
        public int? Occupation { get; set; }
        public int? Education { get; set; }
        public string RiskFactor { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public string BodyMassIndex { get; set; }
        public bool? HasFracture { get; set; }
        public decimal? DXALumber { get; set; }
        public decimal? DXAFemoral { get; set; }
        public decimal? DXATotalHip { get; set; }
        public decimal? BMDLumber { get; set; }
        public decimal? BMDFemoral { get; set; }
        public decimal? BMDTotalHip { get; set; }
        public int? OsteoporosisCase { get; set; }
        public bool? isDeleted { get; set; }
        public int? ServiceCityId { get; set; }
        public int? LiveCityId { get; set; }
        public string FileUrl { get; set; }
        public int FromAge { get; set; }
        public int ToAge { get; set; }
        public int FileType { get; set; }  // if(pdf){1}  // if(excel){2} // if(word){3}
    }

    public class PatientViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientFatherName { get; set; }
        public string PatientLastName { get; set; }
        public int ServiceCityId { get; set; }
        public int LiveCityId { get; set; }
        public string ServiceCity { get; set; }
        public string LiveCity { get; set; }
        public string OsteoporosisCase { get; set; }
        public string NationalId { get; set; }
        public bool isDeleted { get; set; }
        public string FileUrl { get; set; }
        public bool PatientGender { get; set; }
        public DateTime TestDate { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime CreationDate { get; set; }
        public string ContactNumber { get; set; }
        public string MobileNumber { get; set; }
        public int Occupation { get; set; }
        public int Education { get; set; }
        public string RiskFactor { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public string BodyMassIndex { get; set; }
        public bool HasFracture { get; set; }
        public decimal DXALumber { get; set; }
        public decimal DXAFemoral { get; set; }
        public decimal DXATotalHip { get; set; }
        public decimal BMDLumber { get; set; }
        public decimal BMDFemoral { get; set; }
        public decimal BMDTotalHip { get; set; }
    }

    public class UserActionViewModel
    {
        public string ActionDetails { get; set; }
        public DateTime ActionDate { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
    }

}
