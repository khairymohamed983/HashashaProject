﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HashashaSPA.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [MaxLength(250), Required]
        public string FullName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastLogin { get; set; }
        public bool isDeleted { get; set; }
        public string JobTitle { get; set; }
        public double HealthFacility { get; set; }


        public int ServiceCityId { get; set; }
        public virtual City ServiceCity { get; set; }

        // relation between ApplicationUser and userAction
        public virtual ICollection<UserAction> UserActions { get; set; }
        public virtual ICollection<UserPrivilage> UserPrivilages { get; set; }

        public virtual ICollection<Patient> Patients { get; set; }
    }

    public class Patient
    {
        [Key]
        public int PatientId { get; set; }
        [MaxLength(250), Required]
        public string PatientFirstName { get; set; }
        [MaxLength(250), Required]
        public string PatientFatherName { get; set; }
        [MaxLength(250), Required]
        public string PatientLastName { get; set; }
        public bool PatientGender { get; set; }
        [MaxLength(100), Required]
        public string NationalId { get; set; }
        public DateTime TestDate { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime CreationDate { get; set; }
        [MaxLength(50), Required]
        public string ContactNumber { get; set; }
        [MaxLength(50), Required]
        public string MobileNumber { get; set; }
        public int Occupation { get; set; }
        public int Education { get; set; }
        [MaxLength(50), Required]
        public string RiskFactor { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        // [MaxLength(100), Required]
        public string BodyMassIndex { get; set; }
        public bool HasFracture { get; set; }
        public decimal DXALumber { get; set; }
        public decimal DXAFemoral { get; set; }
        public decimal DXATotalHip { get; set; }
        public decimal BMDLumber { get; set; }
        public decimal BMDFemoral { get; set; }
        public decimal BMDTotalHip { get; set; }
        public int OsteoporosisCase { get; set; }
        public bool isDeleted { get; set; }
        public string FileUrl { get; set; }

        // relation between city and patient

        public int ServiceCityId { get; set; }
        public virtual City ServiceCity { get; set; }
        public int LiveCityId { get; set; }
        public virtual City LiveCity { get; set; }

        // relation with patient and ApplicationUser
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

    }

    public class City
    {
        public City()
        {
            LivePatients = new List<Patient>();
            ServicePatients = new List<Patient>();
            ApplicationUsers = new List<ApplicationUser>();
        }
        [Key]
        public int CityId { get; set; }
        [MaxLength(250), Required]
        public string CityName { get; set; }
        public bool isDeleted { get; set; }

        // relation between city and patient
        public virtual ICollection<Patient> LivePatients { get; set; }
        public virtual ICollection<Patient> ServicePatients { get; set; }

        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }

    }

    public class UserAction
    {
        [Key]
        public int ActionId { get; set; }
        [MaxLength(500)]
        public string ActionDetails { get; set; }
        public DateTime ActionDate { get; set; }
        public bool isDeleted { get; set; }
        public string Id { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

    }

    public class Privilage
    {
        [Key]
        public int PrivilageId { get; set; }
        [MaxLength(250), Required]
        public string PrivilageName { get; set; }

        // many to many with application roles
        public virtual ICollection<RolePrivilage> RolePrivilages { get; set; }

        public virtual ICollection<UserPrivilage> UserPrivilages { get; set; }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base()
        {
        }
        public ApplicationRole(string name) : base(name) { }
        public bool isDeleted { get; set; }

        public virtual ICollection<RolePrivilage> RolePrivilages { get; set; }
    }

    public class RolePrivilage
    {
        // Set the column order so it appears nice in the database
        [Key, Column(Order = 0)]
        public string Id { get; set; }
        [Key, Column(Order = 1)]
        public int PrivilageId { get; set; }

        // Add any additional fields you need
        public bool isAllowed { get; set; }

        // Add the navigation properties
        public virtual Privilage Privilage { get; set; }
        public virtual ApplicationRole ApplicationRole { get; set; }
    }

    public class UserPrivilage
    {
        // Set the column order so it appears nice in the database
        [Key, Column(Order = 0)]
        public string Id { get; set; }
        [Key, Column(Order = 1)]
        public int PrivilageId { get; set; }

        // Add any additional fields you need
        public bool isAllowed { get; set; }

        // Add the navigation properties
        public virtual Privilage Privilage { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class ApplicationUserRole : IdentityUserRole
    {

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("HashContext", throwIfV1Schema: false)
        {
        }

        //Override default table names
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // relation between ApplicationUser and userAction
            modelBuilder.Entity<UserAction>()
                    .HasRequired(s => s.ApplicationUser)
                    .WithMany(s => s.UserActions)
                    .HasForeignKey(s => s.Id);

            // relation between ApplicationUser and userAction 
            modelBuilder.Entity<City>()
               .HasMany(e => e.ServicePatients)
               .WithRequired(e => e.ServiceCity)
               .HasForeignKey(e => e.ServiceCityId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<City>()
            .HasMany(e => e.LivePatients)
            .WithRequired(e => e.LiveCity)
            .HasForeignKey(e => e.LiveCityId)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>().Property(x => x.Email).IsOptional();

            base.OnModelCreating(modelBuilder);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<UserAction> UserActions { get; set; }
        public DbSet<Privilage> Privilages { get; set; }
        public DbSet<UserPrivilage> UserPrivilages { set; get; }
        public DbSet<RolePrivilage> RolePrivilages { set; get; }
        public DbSet<ApplicationUserRole> ApplicationUserRoles { get; set; }
        public DbSet<ApplicationRole> Roles { set; get; }
    }
}